package views;

import engine.MusicPlayer;
import engine.SinglePlayer;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;

class JPanelIntro extends JPanel {

    private final MainWindow mWindow;
    private final MusicPlayer mPlayer;
    private final String mHost;
    private final int mPort;

    JPanelIntro(final MainWindow window, final MusicPlayer player, final String host, final int port) {
        mWindow = window;
        mPlayer = player;
        mHost = host;
        mPort = port;
        JButton single = new JButton("Single Player");
        single.addActionListener(actionEvent -> singlePlayer());
        JButton coop = new JButton("Coop Mode");
        coop.addActionListener(actionEvent -> coopMode());
        JButton oneVsOne = new JButton("1 vs 1");
        oneVsOne.addActionListener(actionEvent -> playerVsPlayer());

        BoxLayout box = new BoxLayout(this, BoxLayout.Y_AXIS);
        setLayout(box);
        add(Box.createVerticalGlue());
        add(single);
        add(coop);
        add(oneVsOne);
        single.setFocusable(false);
        coop.setFocusable(false);
        oneVsOne.setFocusable(false);
        // mPlayer.intro();
    }

    private void singlePlayer() {
        try {
            SinglePlayer game = new SinglePlayer(mHost, mPort);
            System.out.println(game.start());
            mWindow.updateJPanel(new JPanelSinglePlayer(mWindow), new BorderLayout());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        System.out.println("Single Player");
    }

    private void coopMode() {
        // mPlayer.test();
        System.out.println("Coop Mode");
    }

    private void playerVsPlayer() {
        System.out.println("1 vs 1");
    }
}
