package views;

import javax.swing.*;
import java.awt.*;

class JPanelSinglePlayer extends JPanel {

    private final MainWindow mWindow;

    JPanelSinglePlayer(final MainWindow window) {
        mWindow = window;
        setBackground(Color.RED);
        setSize(new Dimension(800, 600));
        JPanel grid = new JPanel();
        grid.setSize(new Dimension(600, 600));
        grid.setBackground(Color.BLUE);
        add(grid);
        JPanel settings = new JPanel();
        settings.setSize(new Dimension(200, 600));
        settings.setBackground(Color.GRAY);
        add(settings);
    }
}
