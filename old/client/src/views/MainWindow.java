package views;

import engine.MusicPlayer;

import javax.swing.*;
import java.awt.*;

public class MainWindow extends JFrame {

    public MainWindow(final String name, final MusicPlayer player, final String host, final int port) {
        super(name);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        JPanelIntro intro = new JPanelIntro(this, player, host, port);
        setLayout(new GridBagLayout());
        setPreferredSize(new Dimension(800, 600));
        getContentPane().add(intro, new GridBagConstraints());
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    public void updateJPanel(final JPanel panel, final Object constraints) {
        setVisible(false);
        getContentPane().removeAll();
        getContentPane().add(panel);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }
}
