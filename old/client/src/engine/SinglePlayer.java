package engine;

import org.json.JSONObject;

import java.io.*;
import java.net.Socket;

public class SinglePlayer {

    private final PrintWriter mOut;
    private final BufferedReader mIn;

    public SinglePlayer(final String host, final int port) throws IOException {
        Socket socket = new Socket(host, port);
        mOut = new PrintWriter(socket.getOutputStream(), true);
        mIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        JSONObject message = new JSONObject();
        message.put("game_type", "single");
        mOut.println(message.toString());
        mOut.flush();
    }

    public String start() throws IOException {
        JSONObject obj = new JSONObject();
        obj.put("request_type", "start");
        JSONObject obj2 = new JSONObject();
        obj.put("request", obj2.toString());
        return sendAndReceive(obj.toString());
    }

    private String sendAndReceive(final String message) throws IOException {
        System.out.println(message);
        send(message);
        return mIn.readLine();
    }

    private void send(final String message) throws IOException {
        mOut.println(message);
        mOut.flush();
    }

}
