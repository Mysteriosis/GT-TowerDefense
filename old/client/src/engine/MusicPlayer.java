package engine;

import sun.audio.AudioPlayer;
import sun.audio.ContinuousAudioDataStream;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public class MusicPlayer {

    private ContinuousAudioDataStream mAudio;

    public void intro() {
        play("/resources/music/intro.wav");

    }

    public void test() {
        play("/resources/music/intro.wav");
    }

    private void play(final String path) {
        try {
            // TODO: find a way to play music and stop it !
    //        stop();
            System.out.println(path);
            System.out.println(getClass().getResource(path));
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(getClass().getResource(path));
            Clip clip = AudioSystem.getClip();
            clip.open(audioInputStream);
            clip.loop(Clip.LOOP_CONTINUOUSLY);
            clip.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void stop() {
        if (mAudio != null) {
            AudioPlayer.player.stop(mAudio);
        }
    }
}
