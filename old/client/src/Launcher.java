import engine.MusicPlayer;
import views.MainWindow;

public class Launcher {

    public static void main(String... args) {
        MusicPlayer player = new MusicPlayer();
        new MainWindow("Tower Defense", player, "127.0.0.1", 1337);
    }
}
