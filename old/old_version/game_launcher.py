#!/usr/bin/env python3
# -*- coding:utf-8 -*-
from argparse import ArgumentParser
from flask import Flask, render_template, jsonify, request
from flask_socketio import SocketIO
from game_engine import Engine

async_mode = None
app = Flask(__name__, template_folder='views/templates', static_folder='views/static', static_url_path='')
app.config['SECRET_KEY'] = 'secret!'
socket = SocketIO(app, async_mode=async_mode)
thread = None
engine = None
msg_game_type_error = {'status': 'ERROR', 'message': 'Argument game_type is missing, must be `single` or `multi` !'}
msg_game_not_started = {'status': 'ERROR', 'message': 'No game previously started !'}
msg_game_already_started = {'status': 'ERROR', 'message': 'Game already started !'}


def background_thread(game_type):
    global engine
    engine = Engine(socket=socket, game_type=game_type, namespace='/ws')
    engine.start_game()


@app.route('/', methods=['GET'])
def index():
    return render_template('index.html', async_mode=socket.async_mode)


@app.route('/start', methods=['GET'])
def start_game():
    global thread
    game_type = request.args.get('game_type')
    if game_type is None:
        return jsonify(msg_game_type_error)
    if game_type == 'single' or game_type == 'multi':
        if game_type == 'multi':
            return jsonify({'status': 'OK', 'message': 'This game feature is currently not implemented !'})
        if thread is None:
            thread = socket.start_background_task(target=background_thread, game_type=game_type)
            return jsonify({'status': 'OK', 'message': game_type + ' player game successfully started !'})
        return jsonify(msg_game_already_started)
    return jsonify(msg_game_type_error)


@app.route('/stop', methods=['GET'])
def stop_game():
    global thread, engine
    if engine is not None:
        engine.stop_game()
        thread, engine = None, None
        return jsonify({'status': 'OK', 'message': 'Game successfully stopped !'})
    return jsonify(msg_game_not_started)


@app.route('/build_tower', methods=['POST'])
def build_tower():
    global engine
    if engine is not None:
        return jsonify(engine.build_tower(request.get_json()))
    return jsonify(msg_game_not_started)


@app.route('/remove_tower', methods=['POST'])
def remove_tower():
    global engine
    if engine is not None:
        return jsonify(engine.remove_tower(request.get_json()))
    return jsonify(msg_game_not_started)


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('-port', metavar='<PORT>', type=int, default=8080, help='The server port number, default 8080.')
    socket.run(app, host='0.0.0.0', port=parser.parse_args().port)
