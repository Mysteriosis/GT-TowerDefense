#!/usr/bin/env bash
if [ `id | sed -e 's/(.*//'` != "uid=0" ]; then
  echo "Sorry, you need super user privileges to run this script."
  exit 1
fi
apt install -y python3-dev python3-pip
pip3 install --upgrade pip
pip3 install --upgrade flask
pip3 install --upgrade flask-socketio