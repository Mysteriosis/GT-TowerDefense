# -*- coding:utf-8 -*-
from game_settings import initial_money, initial_lives, initial_wave, initial_x_position, initial_y_position, \
    towers_type, monsters_type, waves_order, waves, grid
from time import sleep


class Engine(object):
    def __init__(self, socket, game_type, namespace):
        self.__socket = socket
        self.__game_type = game_type
        self.__namespace = namespace
        self.__is_running = False
        self.__money = initial_money
        self.__lives = initial_lives
        self.__next_wave = initial_wave
        self.__x = initial_x_position
        self.__y = initial_y_position
        self.__grid = grid
        self.__towers = {}
        self.__monsters = list()
        self.__monsters_id = 0
        self.__t_websocket = None
        self.__game_event = {}

    def start_game(self):
        self.__is_running = True
        message = 'Game starting'
        player = self.__get_player_data()
        current_grid = self.__grid
        message = {'status': 'OK', 'message': message, 'player': player, 'grid': current_grid}
        self.__socket.emit('game_starting', message, namespace=self.__namespace, json=True)
        self.__socket.sleep(5)
        self.__monsters_id = 0
        if self.__game_type == 'multi':
            self.__multi_player_game()
        else:
            self.__single_player_game()

    def stop_game(self):
        """ OK """
        self.__is_running = False
        if self.__t_websocket is not None:
            self.__t_websocket.join()
            self.__t_websocket = None

    def __game_lost(self):
        """ OK """
        self.stop_game()
        message = {'status': 'OK', 'message': 'Game lost !'}
        self.__socket.emit('game_ending', message, namespace=self.__namespace, json=True)
        print('Game lost !')

    def __game_won(self):
        """ OK """
        self.stop_game()
        message = {'status': 'OK', 'message': 'Game won !'}
        self.__socket.emit('game_ending', message, namespace=self.__namespace, json=True)
        print('Game won !')

    def __single_player_game(self):
        sleep(5)
        self.__send_wave()
        while self.__is_running:
            sleep(5)
            self.__next_wave = waves_order[self.__next_wave]
            if self.__next_wave is not None:
                self.__send_wave()
            else:
                self.__game_won()
                return

    def __multi_player_game(self):
        # TODO: Not yet implemented, should not happen yet !
        pass

    def __get_player_data(self):
        """ OK """
        return {'money': self.__money, 'lives': self.__lives}

    def build_tower(self, json):
        if 'x' not in json.keys() or 'y' not in json.keys() or 'tower_type' not in json.keys():
            return {'status': 'ERROR', 'message': 'Malformed JSON -> {`tower_type`: ..., `x`: ..., `y`: ...}'}
        tower_type = json['tower_type']
        if tower_type not in towers_type.keys():
            return {'status': 'ERROR', 'message': 'This tower does not exist !'}
        x = json['x']
        y = json['y']
        if x >= len(self.__grid) or y >= len(self.__grid[x]):
            return {'status': 'ERROR', 'message': 'X or Y coordinates out of bound !'}
        # TODO: Check if trying to build upgraded tower directly
        # TODO: Check upgrade tower if previous is the correct one
        if self.__grid[x][y] != 0:
            return {'status': 'ERROR', 'message': 'Cell not available !'}
        cost = towers_type[tower_type]['cost']
        if self.__money < cost:
            return {'status': 'ERROR', 'message': 'Not enough money !'}
        self.__money -= cost
        message = {'status': 'OK', 'type': 'money', 'amount': -cost}
        self.__socket.emit('game_event', message, namespace=self.__namespace, json=True)
        self.__grid[x][y] = 2
        self.__towers[(x, y)] = tower_type
        return {'status': 'OK', 'message': 'Tower built !'}

    def __compute_total_cost(self, tower_type, ratio):
        """ OK """
        if tower_type is None:
            return 0
        else:
            cost = int(towers_type[tower_type]['cost'] * ratio)
            return cost + self.__compute_total_cost(towers_type[tower_type]['previous'], ratio)

    def remove_tower(self, json):
        """ OK """
        if 'x' not in json.keys() or 'y' not in json.keys():
            return {'status': 'ERROR', 'message': 'Malformed JSON -> {`x`: ..., `y`: ...}'}
        x = json['x']
        y = json['y']
        if x >= len(self.__grid) or y >= len(self.__grid[x]):
            return {'status': 'ERROR', 'message': 'X or Y coordinates out of bound !'}
        if self.__grid[x][y] != 2:
            return {'status': 'ERROR', 'message': 'No tower to remove !'}
        tower_type = self.__towers[(x, y)]
        total_cost = self.__compute_total_cost(tower_type, 0.75)
        self.__money += total_cost
        message = {'status': 'OK', 'type': 'money', 'amount': total_cost}
        self.__socket.emit('game_event', message, namespace=self.__namespace, json=True)
        self.__grid[x][y] = 0
        del self.__towers[(x, y)]
        return {'status': 'OK', 'message': 'Tower removed !'}

    def __send_wave(self):
        """ OK """
        message = {'status': 'OK', 'message': 'Wave started !'}
        self.__socket.emit('game_ending', message, namespace=self.__namespace, json=True)
        wave = waves[self.__next_wave]
        for monsters in wave:
            for _ in range(monsters[1]):
                monster = monsters_type[monsters[0]]
                m = (self.__monsters_id, self.__x, self.__y, monster['health'], monster['speed'], monster['money'],
                     monster['damage'], None)
                self.__monsters_id += 1
                self.__monsters.append(m)
                sleep(0.2)
                self.__fire_towers()
                self.__move_monsters()
                if self.__lives <= 0:
                    self.__game_lost()
                    return
        while len(self.__monsters) != 0:
            sleep(0.2)
            self.__fire_towers()
            self.__move_monsters()
            if self.__lives <= 0:
                self.__game_lost()
                return
        message = {'status': 'OK', 'message': 'Wave ended !'}
        self.__socket.emit('game_ending', message, namespace=self.__namespace, json=True)

    def __move_monsters(self):
        """ OK """
        monsters_to_remove = list()
        for i, m in enumerate(self.__monsters):
            x = m[1]
            y = m[2]
            x_min = 2
            y_min = 2
            x_max = len(self.__grid) - 2
            y_max = len(self.__grid[0]) - 2
            last_move = m[7]
            # Center monster:
            if y >= y_min - 1 and (self.__grid[x][y - 1] == 0 or self.__grid[x][y - 1] == 2):
                y += 1
            elif y <= y_max + 1 and (self.__grid[x][y + 1] == 0 or self.__grid[x][y + 1] == 2):
                y -= 1
            self.__monsters[i] = (m[0], x, y, m[3], m[4], m[5], m[6], last_move)
            # Move monster:
            for _ in range(m[4]):
                if x == 1:
                    x -= 1
                    self.__monsters[i] = (m[0], x, y, m[3], m[4], m[5], m[6], last_move)
                    message = {'status': 'OK', 'type': 'monster', 'monster_id': m[0], 'x': x, 'y': y}
                    self.__socket.emit('game_event', message, namespace=self.__namespace, json=True)
                elif x == 0:
                    self.__lives -= m[6]
                    message = {'status': 'OK', 'type': 'lives', 'amount': -m[6]}
                    self.__socket.emit('game_event', message, namespace=self.__namespace, json=True)
                    monsters_to_remove.append(i)
                    break
                else:
                    if last_move is None or last_move == 'U' or last_move == 'R':
                        if x >= x_min and self.__grid[x - 1][y] == 1 and self.__grid[x - 2][y] == 1:
                            x -= 1
                            last_move = 'U'
                        elif y <= y_max and self.__grid[x][y + 1] == 1 and self.__grid[x][y + 2] == 1:
                            y += 1
                            last_move = 'R'
                        elif y >= y_min and self.__grid[x][y - 1] == 1 and self.__grid[x][y - 2] == 1:
                            y -= 1
                            last_move = 'L'
                        elif x <= x_max and self.__grid[x - 1][y] == 1 and self.__grid[x - 2][y] == 1:
                            x -= 1
                            last_move = 'D'
                    elif last_move == 'L':
                        if x >= x_min and self.__grid[x - 1][y] == 1 and self.__grid[x - 2][y] == 1:
                            x -= 1
                            last_move = 'U'
                        elif y >= y_min and self.__grid[x][y - 1] == 1 and self.__grid[x][y - 2] == 1:
                            y -= 1
                            last_move = 'L'
                        elif y <= y_max and self.__grid[x][y + 1] == 1 and self.__grid[x][y + 2] == 1:
                            y += 1
                            last_move = 'R'
                        elif x <= x_max and self.__grid[x - 1][y] == 1 and self.__grid[x - 2][y] == 1:
                            x -= 1
                            last_move = 'D'
                    else:
                        if x <= x_max and self.__grid[x - 1][y] == 1 and self.__grid[x - 2][y] == 1:
                            x -= 1
                            last_move = 'D'
                        elif y >= y_min and self.__grid[x][y - 1] == 1 and self.__grid[x][y - 2] == 1:
                            y -= 1
                            last_move = 'L'
                        elif y <= y_max and self.__grid[x][y + 1] == 1 and self.__grid[x][y + 2] == 1:
                            y += 1
                            last_move = 'R'
                        elif x >= x_min and self.__grid[x - 1][y] == 1 and self.__grid[x - 2][y] == 1:
                            x -= 1
                            last_move = 'U'
                    self.__monsters[i] = (m[0], x, y, m[3], m[4], m[5], m[6], last_move)
                    message = {'status': 'OK', 'type': 'monster', 'monster_id': m[0], 'x': x, 'y': y}
                    self.__socket.emit('game_event', message, namespace=self.__namespace, json=True)
        monsters_to_remove.reverse()
        for i in monsters_to_remove:
            del self.__monsters[i]

    def __fire_towers(self):
        towers = self.__towers.keys()
        for (x, y) in towers:
            _tower_type = self.__towers[(x, y)]
            _tower = towers_type[_tower_type]
            _range = _tower['range']
            _speed = _tower['speed']
            _power = _tower['power']
            _shots = 0
            while _shots < _speed and len(self.__monsters) != 0:
                monsters_to_remove = list()
                for (i, m) in enumerate(self.__monsters):
                    _shots += 1
                    monster_health = m[3]
                    if (x - _range) <= m[1] <= (x + _range) and (y - _range) <= m[2] <= (y + _range):
                        monster_health -= _power
                        self.__monsters[i] = (m[0], m[1], m[2], monster_health, m[4], m[5], m[6], m[7])
                        message = {'status': 'OK', 'type': 'tower', 'monster_id': m[0], 'x': x, 'y': y}
                        self.__socket.emit('game_event', message, namespace=self.__namespace, json=True)
                        if monster_health <= 0:
                            monsters_to_remove.append(i)
                            self.__money += m[5]
                            break
                    if _shots >= _speed:
                        break
                monsters_to_remove.reverse()
                for i in monsters_to_remove:
                    del self.__monsters[i]
