package interfaces;

public interface IModel {

    int getAbsoluteX();
    int getAbsoluteY();
}
