package controllers;

import javafx.scene.input.KeyEvent;
import services.Coordinate;
import models.Projectile;
import controllers.engine.Toast;
import interfaces.IController;
import javafx.animation.PathTransition;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class SinglePlayerController implements IController {

    private PrintWriter mOut;
    private BufferedReader mIn;
    private Thread thread;
    private boolean mIsRunning = false;
    private Stage mStage;
    private Pane[][] mGridPaneArray;
    private int[][] mGrid;
    private Map<Integer, Coordinate> monsters = new HashMap<>();
    private final LinkedList<JSONObject> mBuffer = new LinkedList<>();
    private KeyEvent mCurrentKeyEvent;
    private Runnable listener = new Runnable() {
        @Override
        public void run() {
            try {
                mIsRunning = true;
                String line;
                while (mIsRunning) {
                    line = mIn.readLine();
                    if (line == null) {
                        mIsRunning = false;
                        break;
                    }
                    if (mIsRunning && (!line.equals("\n") || !line.equals(""))) {
                        try {
                            line = line.replace("\n", "").replace("\r", "");
                            JSONObject response = new JSONObject(line);
                            synchronized (mBuffer) {
                                mBuffer.add(response);
                            }
                        } catch (JSONException e) {
                            System.out.println(e.getMessage());
                        }
                    }
                }
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }
    };

    @Override
    public void setHostAndPort(final String host, final int port, final String gameType) throws IOException {
        Socket socket = new Socket(host, port);
        mOut = new PrintWriter(socket.getOutputStream(), true);
        mIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        initializeGame(gameType);
    }

    private void initializeGame(String gameType) throws IOException {
        thread = new Thread(listener);
        thread.start();
        JSONObject message = new JSONObject();
        message.put("game_type", gameType);
        send(message);
    }

    private void send(JSONObject obj) throws IOException {
        mOut.println(obj.toString());
        mOut.flush();
    }

    private void initializeView(JSONObject object) throws JSONException {
        GridPane gridPane = new GridPane();
        Group tilemapGroup = new Group();
        JSONObject obj = object.getJSONObject("message");
        lives.setText("Lives: " + obj.getString("lives"));
        money.setText("Money: " + obj.getString("money"));
        JSONArray array = obj.getJSONArray("grid");
        mGrid = new int[array.length()][array.getJSONArray(0).length()];
        for (int i = 0; i < array.length(); ++i) {
            JSONArray sub = array.getJSONArray(i);
            for (int j = 0; j < sub.length(); ++j) {
                mGrid[i][j] = sub.getInt(j);
            }
        }
        gridPane.getStyleClass().add("game-grid");

        for(int i = 0; i < 60; i++) {
            ColumnConstraints column = new ColumnConstraints(15);
            gridPane.getColumnConstraints().add(column);
        }

        for(int i = 0; i < 60; i++) {
            RowConstraints row = new RowConstraints(15);
            gridPane.getRowConstraints().add(row);
        }
        gridPane.getStylesheets().add("resources/grid-with-borders.css");
        mStage.getScene().setOnKeyTyped((keyEvent) -> {
                mCurrentKeyEvent = keyEvent;
        });
        gridPane.setOnMouseClicked((mouseEvent) -> {
                Coordinate coord = new Coordinate(mouseEvent.getY(), mouseEvent.getX());
                if (mCurrentKeyEvent != null) {
                    String c = mCurrentKeyEvent.getCharacter();
                    String tower = c.equals("5") ? "tower_41" : c.equals("4") ? "tower_31" : c.equals("3") ? "tower_21" : c.equals("2") ? "tower_11" : "tower_1";
                    try {
                        JSONObject message = new JSONObject();
                        message.put("request_type", "build_tower");
                        JSONObject request = new JSONObject();
                        request.put("tower_type", tower);
                        request.put("x", coord.getTileX());
                        request.put("y", coord.getTileY());
                        message.put("request", request);
                        send(message);
                    } catch (IOException e) {
                        System.out.println(e.getMessage());
                    }
                }
        });
        mGridPaneArray = new Pane[60][60];
        for (int i = 0; i < 60; i++) {
            for (int j = 0; j < 60; j++) {
                Pane pane = new Pane();
                gridPane.add(pane, j, i);
                mGridPaneArray[i][j] = pane;
            }
        }
        projectileGroup.getChildren().add(tilemapGroup);
        tilemapGroup.getChildren().add(gridPane);
        projectileGroup.setPickOnBounds(false);
        stack.getChildren().add(projectileGroup);
        refreshGUI();
    }

    @FXML private StackPane stack;
    @FXML private Label lives;
    @FXML private Label money;
    @FXML private Group projectileGroup;
    public void initialize(){
        executeResponse();
    }

    private void executeResponse() {
        if (mBuffer.isEmpty()) {
            Task<Void> sleeper = new Task<Void>() {
                @Override
                protected Void call() throws Exception {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        System.out.println(e.getMessage());
                    }
                    return null;
                }
            };
            sleeper.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
                @Override
                public void handle(WorkerStateEvent event) {
                    executeResponse();
                }
            });
            new Thread(sleeper).start();
        } else {
            JSONObject object;
            synchronized (mBuffer) {
                object = mBuffer.removeFirst();
            }
            try {
                parseResponse(object);
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }

    }

    private void parseResponse(JSONObject response) throws IOException {
        String key = "message";
        String toast;
        JSONObject message;
        switch (response.getString("message_type")) {
            case "info":
                switch (response.getString(key)) {
                    case "player_0":
                        System.out.println("player_0");
                        break;
                    case "player_1":
                        System.out.println("player_1");
                        break;
                    case "player_2":
                        System.out.println("player_2");
                        break;
                }
                message = new JSONObject();
                message.put("request_type", "start");
                message.put("request", "{}");
                send(message);
                break;
            case "initialization":
                /* OK */
                initializeView(response);
                break;
            case "wave_started":
                toast = "Wave started : " + response.getString(key).replace("wave_", "");
                Toast.makeText(mStage, toast, 1000, 250, 250);
                break;
            case "wave_ended":
                toast = "Wave ended !";
                Toast.makeText(mStage, toast, 1000, 250, 250);
                break;
            case "game_started":
                toast = "Game started !";
                Toast.makeText(mStage, toast, 1000, 250, 250);
                break;
            case "game_stopped":
                toast = "Game stopped !";
                Toast.makeText(mStage, toast, 1000, 250, 250);
                break;
            case "game_paused":
                toast = "Game paused !";
                Toast.makeText(mStage, toast, 1000, 250, 250);
                break;
            case "game_resumed":
                toast = "Game resumed !";
                Toast.makeText(mStage, toast, 1000, 250, 250);
                break;
            case "game_lost":
                toast = "Game lost !";
                Toast.makeText(mStage, toast, 3000, 250, 250);
                break;
            case "game_won":
                toast = "Game won !";
                Toast.makeText(mStage, toast, 3000, 250, 250);
                break;
            case "monster_move":
                JSONArray moves = response.getJSONArray(key);
                for (int i = 0; i < moves.length(); ++i) {
                    message = moves.getJSONObject(i);
                    final int id = message.getInt("id");
                    final int last_x = message.getInt("last_x");
                    final int last_y = message.getInt("last_y");
                    final int next_x = message.getInt("x");
                    final int next_y = message.getInt("y");
                    final Coordinate coordinates = new Coordinate(next_x, next_y);
                    monsters.put(id, coordinates);
                    mGrid[next_x][next_y] = 3;
                    mGrid[last_x][last_y] = 1;
                }
                refreshGUI();
                break;
            case "monster_hit":
                /* OK */
                System.out.println(response);
                JSONArray array = response.getJSONArray(key);
                Projectile projectile;
                Path projectilePath;
                PathTransition animation;
                for (int i = 0; i < array.length(); ++i) {
                    JSONObject obj = array.getJSONObject(i);
                    int towerY = obj.getJSONObject("tower").getInt("x") * 15 + 25;
                    int towerX = obj.getJSONObject("tower").getInt("y") * 15 + 25;
                    int monsterY = obj.getJSONObject("monster").getInt("x") * 15 + 17;
                    int monsterX = obj.getJSONObject("monster").getInt("y") * 15 + 17;
                    projectile = new Projectile(monsterX, monsterY, towerX, towerY, new Color(0.5, 0.2, 0.5, 1));
                    projectilePath = new Path(new MoveTo(projectile.getStartX(), projectile.getStartY()));
                    projectilePath.getElements().add(new LineTo(projectile.getEndX(), projectile.getEndY()));
                    animation = new PathTransition(Duration.millis(300), projectilePath, projectile);
                    animation.setOnFinished(new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent actionEvent) {
                            PathTransition finishedAnimation = (PathTransition) actionEvent.getSource();
                            Projectile finishedProjectile = (Projectile) finishedAnimation.getNode();
                            finishedProjectile.setVisible(false);
                            projectileGroup.getChildren().remove(finishedProjectile);

                        }
                    });
                    projectileGroup.getChildren().add(projectile);
                    animation.play();
                }
                break;
            case "monster_death":
                final int id = response.getJSONObject(key).getInt("id");
                monsters.remove(id);
                final int x = response.getJSONObject(key).getInt("x");
                final int y = response.getJSONObject(key).getInt("y");
                mGrid[x][y] = 1;
                refreshGUI();
                break;
            case "tower_build":
                message = response.getJSONObject(key);
                final int next_x = message.getInt("x");
                final int next_y = message.getInt("y");
                final String tower = message.getString("tower_type");
                switch(tower) {
                    case "tower_1":
                        mGrid[next_x][next_y] = 4;
                        mGrid[next_x + 1][next_y] = 4;
                        mGrid[next_x][next_y + 1] = 4;
                        mGrid[next_x + 1][next_y + 1] = 4;
                        break;
                    case "tower_11":
                        mGrid[next_x][next_y] = 5;
                        mGrid[next_x + 1][next_y] = 5;
                        mGrid[next_x][next_y + 1] = 5;
                        mGrid[next_x + 1][next_y + 1] = 5;
                        break;
                    case "tower_21":
                        mGrid[next_x][next_y] = 6;
                        mGrid[next_x + 1][next_y] = 6;
                        mGrid[next_x][next_y + 1] = 6;
                        mGrid[next_x + 1][next_y + 1] = 6;
                        break;
                    case "tower_31":
                        mGrid[next_x][next_y] = 7;
                        mGrid[next_x + 1][next_y] = 7;
                        mGrid[next_x][next_y + 1] = 7;
                        mGrid[next_x + 1][next_y + 1] = 7;
                        break;
                    case "tower_41":
                        mGrid[next_x][next_y] = 8;
                        mGrid[next_x + 1][next_y] = 8;
                        mGrid[next_x][next_y + 1] = 8;
                        mGrid[next_x + 1][next_y + 1] = 8;
                        break;
                }
                refreshGUI();
                break;
            case "tower_update":
                System.out.println(response);
                break;
            case "tower_delete":
                System.out.println(response);
                break;
            case "money":
                /* OK */
                money.setText("Money: " + response.get(key));
                break;
            case "lives":
                /* OK */
                lives.setText("Lives: " + response.get(key));
                break;
            case "error":
                /* OK */
                Toast.makeText(mStage, response.getString(key), 2000, 500, 500);
                break;
            default:
                /* OK */
                System.out.println("Unknown type : " + response.getString("message_type"));
        }
        executeResponse();
    }

    private void refreshGUI() {
        for (int i = 0; i < 60; i++) {
            for (int j = 0; j < 60; j++) {
                if (mGrid[i][j] == 0) {
                    mGridPaneArray[i][j].setStyle("-fx-background-color: #B36B00");
                } else if (mGrid[i][j] == 1 || mGrid[i][j] == 2) {
                    mGridPaneArray[i][j].setStyle("-fx-background-color: #FFFFFF");
                } else if (mGrid[i][j] == 3) {
                    mGridPaneArray[i][j].setStyle("-fx-background-color: #FF3300");
                } else if (mGrid[i][j] == 4) {
                    mGridPaneArray[i][j].setStyle("-fx-background-color: #FF66FF");
                } else if (mGrid[i][j] == 5) {
                    mGridPaneArray[i][j].setStyle("-fx-background-color: #0066FF");
                } else if (mGrid[i][j] == 6) {
                    mGridPaneArray[i][j].setStyle("-fx-background-color: #FF6600");
                } else if (mGrid[i][j] == 7) {
                    mGridPaneArray[i][j].setStyle("-fx-background-color: #006666");
                } else if (mGrid[i][j] == 8) {
                    mGridPaneArray[i][j].setStyle("-fx-background-color: #003333");
                }
            }
        }
    }

    @FXML private BorderPane settings;

    @Override
    public void setStage(final Stage stage) {
        mStage = stage;
    }

    @Override
    public void safeClose() {
        System.out.println("Safely closed !");
        mIsRunning = false;
    }
}
