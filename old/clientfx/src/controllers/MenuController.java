package controllers;

import interfaces.IController;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;

import java.io.IOException;

public class MenuController implements IController {

    private String mHost;
    private int mPort;
    private IController mCurrent;
    private String mGameType;
    @Override
    public void setHostAndPort(final String host, final int port, final String gameType) {
        mHost = host;
        mPort = port;
        mGameType = "single";
    }

    private Stage mStage;
    @Override
    public void setStage(final Stage stage) {
        mStage = stage;
    }

    @FXML private void singlePlayer() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/singleplayer.fxml"));
        Parent singleplayer = loader.load();
        mStage.setScene(new Scene(singleplayer));
        SinglePlayerController controller = loader.getController();
        controller.setHostAndPort(mHost, mPort, mGameType);
        controller.setStage(mStage);
        mCurrent = controller;
        mStage.setResizable(false);
        Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
        mStage.setX((primScreenBounds.getWidth() - mStage.getWidth()) / 2);
        mStage.setY((primScreenBounds.getHeight() - mStage.getHeight()) / 2);
        mStage.show();
    }

    @FXML private void coopMode() throws IOException {
        mGameType = "coop";
        singlePlayer();
    }

    @FXML private void oneVsOne() throws IOException {
        System.out.println("One vs One: not yet implemented !");
    }

    @Override
    public void safeClose() {
        if (mCurrent != null) {
            mCurrent.safeClose();
        }
    }
}
