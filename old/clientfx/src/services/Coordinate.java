package services;

public class Coordinate {

    private final int mTileX;
    private final int mTileY;

    public Coordinate(double x, double y) {
        int tmpX = (int) ((x - 10) / 30);
        int tmpY = (int) ((y - 10) / 30);
        mTileX = tmpX <= 0 ? 0 : tmpX >= 29 ? 58 : tmpX * 2;
        mTileY = tmpY <= 0 ? 0 : tmpY >= 29 ? 58 : tmpY * 2;
    }

    public Coordinate(int x, int y) {
        this((double) x, (double) y);
    }

    public int getTileX() {
        return mTileX;
    }

    public int getTileY() {
        return mTileY;
    }

    private boolean equals(Coordinate that) {
        return mTileX == that.mTileX && mTileY == that.mTileY;
    }
}