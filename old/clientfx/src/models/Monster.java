package models;

import interfaces.IModel;
import javafx.scene.image.ImageView;
import services.Coordinate;

public class Monster extends ImageView {

    private Coordinate mNewCoordinate;
    private Coordinate mOldCoordinate;

    public Monster(Coordinate newCoordinate, Coordinate oldCoordinate) {
        mNewCoordinate = newCoordinate;
        mOldCoordinate = oldCoordinate;
    }

    public Coordinate getNewCoordinate() {
        return mNewCoordinate;
    }

    public Coordinate getmOldCoordinate() {
        return mOldCoordinate;
    }
}
