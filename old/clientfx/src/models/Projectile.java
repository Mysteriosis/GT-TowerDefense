package models;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class Projectile extends Circle {
    private final int mEndX;
    private final int mEndY;
    private final int mStartX;
    private final int mStartY;


    public Projectile(int monsterX, int monsterY , int towerX , int towerY , Color color){
        super(towerX , towerY , 5 , color);
        mEndX = monsterX;
        mEndY = monsterY;
        mStartX = towerX;
        mStartY = towerY;
    }


    public int getEndX(){
        return mEndX;
    }

    public int getEndY(){
        return mEndY;
    }

    public int getStartX(){
        return mStartX;
    }

    public int getStartY(){
        return mStartY;
    }



}