import java.io.PrintStream
import java.net.ServerSocket

import engine.GameEngineSinglePlayer
import models.JsonConverter

import scala.io.BufferedSource
import spray.json._
import models.JsonImplicit._
import models.Model.GameType

object Launcher {

  val port = 1337

  private def listener() = {
    val server = new ServerSocket(port)
    while (true) {
      val socket = server.accept()
      val in = new BufferedSource(socket.getInputStream)
      val out = new PrintStream(socket.getOutputStream)
      try {
        in.getLines().next().parseJson.convertTo[GameType].game_type match {
          case "single" =>
            val message = JsonConverter.toJson(Map("message_type" -> "info", "message" -> "Single Player !"))
            out.println(message)
            out.flush()
            GameEngineSinglePlayer(in, out).start()
          case "coop" => println("Coop mode !")
          case "1v1" => println("1v1 mode !")
          case _ => throw new Exception
        }
      } catch {
        case _: Exception =>
          out.println("Invalid game type !")
          out.flush()
          socket.close()
      }
    }
  }

  def main(args: Array[String]): Unit = {
    listener()
  }
}