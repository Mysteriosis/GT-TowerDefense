package models

import spray.json.{DefaultJsonProtocol, JsFalse, JsNumber, JsString, JsTrue, JsValue, JsonFormat, RootJsonFormat}
import models.Model._

object JsonImplicit extends DefaultJsonProtocol {
  implicit val towersFormat: RootJsonFormat[Tower] = jsonFormat7(Tower)
  implicit val monstersFormat: RootJsonFormat[Monster] = jsonFormat5(Monster)
  implicit val monstersInWaveFormat: RootJsonFormat[MonsterInWave] = jsonFormat2(MonsterInWave)
  implicit val wavesFormat: RootJsonFormat[Wave] = jsonFormat2(Wave)
  implicit val gameTypeFormat: RootJsonFormat[GameType] = jsonFormat1(GameType)
  implicit val requestFormat: RootJsonFormat[Request] = jsonFormat2(Request)
  implicit val monsterInGameFormat: RootJsonFormat[MonsterInGame] = jsonFormat11(MonsterInGame)
  implicit val requestBuildFormat: RootJsonFormat[RequestBuild] = jsonFormat3(RequestBuild)
  implicit val requestUpdateFormat: RootJsonFormat[RequestUpdate] = jsonFormat3(RequestUpdate)
  implicit val requestDeleteFormat: RootJsonFormat[RequestDelete] = jsonFormat2(RequestDelete)
  implicit object AnyJsonFormat extends JsonFormat[Any] {
    def write(x: Any): JsValue = x match {
      case n: Int => JsNumber(n)
      case s: String => JsString(s)
      case b: Boolean if b => JsTrue
      case b: Boolean if !b => JsFalse
    }
    def read(value: JsValue): Any = value match {
      case JsNumber(n) => n.intValue()
      case JsString(s) => s
      case JsTrue => true
      case JsFalse => false
    }
  }
}
