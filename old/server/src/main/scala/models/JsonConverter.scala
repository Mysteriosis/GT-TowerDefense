package models
import spray.json.JsValue

import scala.collection.mutable.ListBuffer

object JsonConverter {
  def toJson(o: Any) : String = {
    var json = new ListBuffer[String]()
    o match {
      case m: Map[_,_] =>
        for ( (k,v) <- m ) {
          val key = escape(k.asInstanceOf[String])
          v match {
            case a: Map[_, _] => json += "\"" + key + "\":" + toJson(a)
            case a: List[_] => json += "\"" + key + "\":" + toJson(a)
            case a: Array[_] => json += "\"" + key + "\":" + toJson(a)
            case a: Int => json += "\"" + key + "\":" + a
            case a: Boolean => json += "\"" + key + "\":" + a
            case a: String => json += "\"" + key + "\":\"" + escape(a) + "\""
            case a: JsValue => json += a.toString()
            case _ => ;
          }
        }
      case m: List[_] =>
        var list = new ListBuffer[String]()
        for ( el <- m ) {
          el match {
            case a: Map[_,_] => list += toJson(a)
            case a: List[_] => list += toJson(a)
            case a: Array[_] => list += toJson(a)
            case a: Int => list += a.toString
            case a: Boolean => list += a.toString
            case a: String => list += "\"" + escape(a) + "\""
            case a: JsValue => list += a.toString()
            case _ => ;
          }
        }
        return "[" + list.mkString(",") + "]"
      case m: Array[_] =>
        var list = new ListBuffer[String]()
        for ( el <- m ) {
          el match {
            case a: Map[_,_] => list += toJson(a)
            case a: List[_] => list += toJson(a)
            case a: Array[_] => list += toJson(a)
            case a: Int => list += a.toString
            case a: Boolean => list += a.toString
            case a: String => list += "\"" + escape(a) + "\""
            case a: JsValue => list += a.toString()
            case _ => ;
          }
        }
        return "[" + list.mkString(",") + "]"
      case _ => ;
    }
    "{" + json.mkString(",") + "}"
  }

  private def escape(s: String) : String = {
    s.replaceAll("\"" , "\\\\\"")
  }
}