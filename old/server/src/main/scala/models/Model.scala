package models

import spray.json.JsValue

object Model {

  case class Tower(id: String, cost: Int, power: Int, speed: Int, range: Int, next_upgrade: Option[String], previous: Option[String])

  case class Monster(id: String, health: Int, speed: Int, money: Int, damage: Int)

  case class MonsterInWave(id: String, number: Int)

  case class Wave(id: String, monsters: List[MonsterInWave])

  case class GameType(game_type: String)

  case class Request(request_type: String, request: JsValue)

  case class RequestBuild(tower_type: String, x: Int, y: Int) {
    override def toString: String =
    "{\"tower_type\":\"" + tower_type + "\", \"x\": " + x + ", \"y\": " + y + "}"
  }

  case class RequestUpdate(var tower_type: String, x: Int, y: Int) {
    override def toString: String =
      "{\"tower_type\":\"" + tower_type + "\", \"x\": " + x + ", \"y\": " + y + "}"
  }

  case class RequestDelete(x: Int, y: Int) {
    override def toString: String =
      "{\"x\": " + x + ", \"y\": " + y + "}"
  }

  case class MonsterInGame(id: Int, var last_x: Int, var last_y: Int, var x: Int, var y: Int, var lastMove: String, var currentHealth: Int, health: Int, speed: Int, money: Int, damage: Int) {
    override def toString: String =
      "{\"id\":" + id + ", \"last_x\": " + last_x + ", \"last_y\": " + last_y + ", \"x\": " + x + ", \"y\": " + y + ", \"last_move\" : \"" + lastMove + "\", \"current_health\": " + currentHealth + ", \"health\": " + health + ", \"speed\": " + speed + ", \"damage\": " + damage + "}"
  }
}