package engine

import java.io.PrintStream
import java.net.SocketException

import models.JsonConverter
import models.JsonImplicit._
import models.Model._
import spray.json._

import scala.collection.mutable.ListBuffer
import util.control.Breaks._
import scala.io.BufferedSource

case class GameEngineSinglePlayer(in: BufferedSource, out: PrintStream) extends Thread {

  private val towers = scala.io.Source.fromResource("towers.json").mkString.parseJson.convertTo[List[Tower]].map(t => t.id -> t).toMap
  private val monsters = scala.io.Source.fromResource("monsters.json").mkString.parseJson.convertTo[List[Monster]].map(m => m.id -> m).toMap
  private val waves = scala.io.Source.fromResource("waves.json").mkString.parseJson.convertTo[List[Wave]].map(w => w.id -> w).toMap
  private val orders = scala.io.Source.fromResource("orders.json").mkString.parseJson.convertTo[List[String]]
  private val grid = scala.io.Source.fromResource("grid.json").mkString.parseJson.convertTo[Array[Array[Int]]]
  private val initial_money = 500
  private val initial_lives = 100
  private val initial_x = 59
  private val initial_y = 29

  private val game = new Runnable {
    private var isRunning = true
    private var isPaused = false
    private var monsterId = 0
    private var money = initial_money
    private var lives = initial_lives
    private val monstersList: ListBuffer[MonsterInGame] = ListBuffer()
    private val towersMap: scala.collection.mutable.Map[(Int, Int), Tower] = scala.collection.mutable.Map()
    private val x_min = 2
    private val y_min = 2
    private val x_max = grid.length
    private val y_max = grid.head.length
    override def run(): Unit = {
      breakable {
        val initialization = Map("money" -> initial_money.toString, "lives" -> initial_lives.toString, "grid" -> grid)
        send(JsonConverter.toJson(Map("message_type" -> "initialization", "message" -> initialization)))
        Thread.sleep(5000)
        for (order <- orders) {
          if (!isRunning) break
          checkPause()
          send(JsonConverter.toJson(Map("message_type" -> "wave_started", "message" -> order)))
          waves.get(order) match {
            case Some(wave) =>
              checkPause()
              for (monstersInWave <- wave.monsters) {
                checkPause()
                for (_ <- 0.until(monstersInWave.number)) {
                  checkPause()
                  monsters.get(monstersInWave.id) match {
                    case Some(m) =>
                      checkPause()
                      val _m = MonsterInGame(monsterId, initial_x, initial_y, initial_x, initial_y, "", m.health, m.health, m.speed, m.money, m.damage)
                      monsterId += 1
                      monstersList.append(_m)
                      Thread.sleep(300)
                      fireTowers()
                      moveMonsters()
                      if (lives <= 0) {
                        gameLost()
                        break
                      }
                    case _ => break
                  }
                }
              }
              while (monstersList.nonEmpty) {
                Thread.sleep(300)
                fireTowers()
                moveMonsters()
                if (lives <= 0) {
                  gameLost()
                  break
                }
                if (!isRunning) {
                  break
                }
              }
            case _ => break
          }
          send(JsonConverter.toJson(Map("message_type" -> "wave_ended", "message" -> order)))
          Thread.sleep(5000)
        }
        gameWon()
      }
      println("Game ended !")
    }

    def stopGame(): Unit = {
      isPaused = false
      isRunning = false
    }
    def pauseGame(): Unit = isPaused = true
    def resumeGame(): Unit = isPaused = false

    def buildTower(r: RequestBuild): Unit = {
      val tower = towers.get(r.tower_type)
      tower match {
        case Some(t) =>
          if (r.x >= grid.length || r.y >= grid(r.x).length) {
            send(JsonConverter.toJson(Map("message_type" -> "error", "message" -> "X or Y coordinates out of bound !")))
            return
          }
          if (grid(r.x)(r.y) != 0) {
            send(JsonConverter.toJson(Map("message_type" -> "error", "message" -> "Cell not available !")))
            return
          }
          if (money < t.cost) {
            send(JsonConverter.toJson(Map("message_type" -> "error", "message" -> "Not enough money !")))
            return
          }
          money -= t.cost
          send(JsonConverter.toJson(Map("message_type" -> "money", "message" -> money)))
          grid(r.x)(r.y) = 2
          send("{\"message_type\":\"tower_build\",\"message\":" + r + "}")
          towersMap += (r.x, r.y) -> t
        case _ => send(JsonConverter.toJson(Map("message_type" -> "error", "message" -> "Tower type does not exist !")))
      }
    }

    def updateTower(r: RequestUpdate): Unit = {
      val tower = towers.get(r.tower_type)
      tower match {
        case Some(t) =>
          if (r.x >= grid.length || r.y >= grid(r.x).length) {
            send(JsonConverter.toJson(Map("message_type" -> "error", "message" -> "X or Y coordinates out of bound !")))
            return
          }
          if (grid(r.x)(r.y) != 2) {
            send(JsonConverter.toJson(Map("message_type" -> "error", "message" -> "No tower to upgrade")))
            return
          }
          t.next_upgrade match {
            case Some(nextTower) =>
              towers.get(nextTower) match {
                case Some(_t) =>
                  if (money < _t.cost) {
                    send(JsonConverter.toJson(Map("message_type" -> "error", "message" -> "Not enough money !")))
                    return
                  }
                  money -= _t.cost
                  send(JsonConverter.toJson(Map("message_type" -> "money", "message" -> money)))
                  r.tower_type = nextTower
                  send("{\"message_type\":\"tower_update\",\"message\":" + r + "}")
                  towersMap += (r.x, r.y) -> _t
                case _ => // Should never happen !
              }
            case _ => send(JsonConverter.toJson(Map("message_type" -> "error", "message" -> "No upgrade available")))
          }
        case _ => send(JsonConverter.toJson(Map("message_type" -> "error", "message" -> "Tower type does not exist !")))
      }
    }

    def removeTower(r: RequestDelete): Unit = {
      if (r.x >= grid.length || r.y >= grid(r.x).length) {
        send(JsonConverter.toJson(Map("message_type" -> "error", "message" -> "X or Y coordinates out of bound !")))
        return
      }
      if (grid(r.x)(r.y) != 2) {
        send(JsonConverter.toJson(Map("message_type" -> "error", "message" -> "No tower to remove!")))
        return
      }
      val tower = towersMap.get((r.x, r.y))
      tower match {
        case Some(t) =>
          val cost = computeTotalCost(Some(t.id), 0.75)
          money += cost
          send(JsonConverter.toJson(Map("message_type" -> "money", "message" -> money)))
          grid(r.x)(r.y) = 0
          towersMap.remove((r.x, r.y))
          send("{\"message_type\":\"tower_delete\",\"message\":" + r + "}")
        case _ => // Should never happen !
      }
    }

    private def computeTotalCost(towerType: Option[String], ratio: Double): Int = {
      towerType match {
        case None => 0
        case Some(_type) => towers.get(_type) match {
          case Some(tower) =>
            (tower.cost * ratio).toInt + computeTotalCost(tower.previous, ratio)
          case _ => 0
        }
      }
    }
    private def checkPause(): Unit = while(isPaused) Thread.sleep(100)
    private def moveMonsters(): Unit = {
      checkPause()
      val monstersToRemove: ListBuffer[MonsterInGame] = ListBuffer()
      val monstersMove: ListBuffer[JsValue] = ListBuffer()
      breakable {
        for (m <- monstersList) {
          // Center monster:
          if (m.y >= y_min - 1 && (grid(m.x)(m.y - 1) == 0 || grid(m.x)(m.y - 1) == 2)) {
            m.y += 1
          } else if (m.y <= y_max + 1 && (grid(m.x)(m.y + 1) == 0 || grid(m.x)(m.y + 1) == 2)) {
            m.y -= 1
          }
          for (_ <- 0.until(m.speed)) {
            if (m.x == 1) {
              m.x -= 1
              monstersMove.append(m.copy().toJson)
              m.last_x = m.x
              m.last_y = m.y
            } else if (m.x == 0) {
              lives -= m.damage
              send(JsonConverter.toJson(Map("message_type" -> "lives", "message" -> lives.toString)))
              monstersToRemove.append(m)
              break
            } else {
              // Check turn before everything
              if (m.lastMove == "" || m.lastMove == "U" || m.lastMove == "R") {
                if (m.x >= x_min && grid(m.x - 1)(m.y) == 1 && grid(m.x - 2)(m.y) == 1) {
                  m.x -= 1
                  m.lastMove = "U"
                } else if (m.y <= y_max && grid(m.x)(m.y + 1) == 1 && grid(m.x)(m.y + 2) == 1) {
                  m.y += 1
                  m.lastMove = "R"
                } else if (m.y >= y_min && grid(m.x)(m.y - 1) == 1 && grid(m.x)(m.y - 2) == 1) {
                  m.y -= 1
                  m.lastMove = "L"
                } else if (m.x <= x_max && grid(m.x + 1)(m.y) == 1 && grid(m.x + 2)(m.y) == 1) {
                  m.x += 1
                  m.lastMove = "D"
                }
              } else if (m.lastMove == "L") {
                if (m.x >= x_min && grid(m.x - 1)(m.y) == 1 && grid(m.x - 2)(m.y) == 1) {
                  m.x -= 1
                  m.lastMove = "U"
                } else if (m.y >= y_min && grid(m.x)(m.y - 1) == 1 && grid(m.x)(m.y - 2) == 1) {
                  m.y -= 1
                  m.lastMove = "L"
                } else if (m.y <= y_max && grid(m.x)(m.y + 1) == 1 && grid(m.x)(m.y + 2) == 1) {
                  m.y += 1
                  m.lastMove = "R"
                } else if (m.x <= x_max && grid(m.x + 1)(m.y) == 1 && grid(m.x + 2)(m.y) == 1) {
                  m.x += 1
                  m.lastMove = "D"
                }
              } else {
                if (m.x <= x_max && grid(m.x - 1)(m.y) == 1 && grid(m.x - 2)(m.y) == 1) {
                  m.x -= 1
                  m.lastMove = "D"
                } else if (m.y >= y_min && grid(m.x)(m.y - 1) == 1 && grid(m.x)(m.y - 2) == 1) {
                  m.y -= 1
                  m.lastMove = "L"
                } else if (m.y <= y_max && grid(m.x)(m.y + 1) == 1 && grid(m.x)(m.y + 2) == 1) {
                  m.y += 1
                  m.lastMove = "R"
                } else if (m.x >= x_min && grid(m.x + 1)(m.y) == 1 && grid(m.x + 2)(m.y) == 1) {
                  m.x += 1
                  m.lastMove = "U"
                }
              }
            }
          }
          monstersMove.append(m.copy().toJson)
          m.last_x = m.x
          m.last_y = m.y
        }
      }
      if (monstersMove.toList.nonEmpty) {
        send(JsonConverter.toJson(Map("message_type" -> "monster_move", "message" -> monstersMove.toList)))
      }
      for (m <- monstersToRemove.reverse) {
        monstersList -= m
      }
    }
    private def fireTowers(): Unit = {
      checkPause()
      val towersFire: ListBuffer[JsValue] = ListBuffer()
      for ((x, y) <- towersMap.keySet) {
        val tower = towersMap.get((x, y))
        tower match {
          case Some(t) =>
            var shots = 0
            while (shots < t.speed) {
              Thread.sleep(100)
              val monstersToRemove: ListBuffer[MonsterInGame] = ListBuffer()
              if (monstersList.isEmpty) {
                return
              }
              breakable {
                for (m <- monstersList) {
                  shots += 1
                  if (((x - t.range) <= m.x && m.x <= (x + t.range)) && (((y - t.range) <= m.y) && (m.y <= (y + t.range)))) {
                    m.currentHealth -= t.power

                    towersFire.append(Map("tower" -> Map("tower_type" -> t.id, "x" -> x, "y" -> y).toJson, "monster" -> m.toJson).toJson)
                    if (m.currentHealth <= 0) {
                      monstersToRemove.append(m)
                      money += m.money
                      send(JsonConverter.toJson(Map("message_type" -> "money", "message" -> money.toString)))
                      send("{\"message_type\":\"monster_death\",\"message\":" + m + "}")
                      break
                    }
                  }
                  if (shots >= t.speed) {
                    break
                  }
                }
              }
              for (m <- monstersToRemove.reverse) {
                monstersList -= m
              }
            }
          case _ => // Should never happen !
        }
      }
      if (towersFire.toList.nonEmpty) {
        send(JsonConverter.toJson(Map("message_type" -> "monster_hit", "message" -> towersFire.toList)))
      }
    }
    private def gameLost(): Unit = {
      send(JsonConverter.toJson(Map("message_type" -> "game_lost", "message" -> "Game lost !")))
    }
    private def gameWon(): Unit = {
      send(JsonConverter.toJson(Map("message_type" -> "game_won", "message" -> "Game won !")))
    }
  }

  private def clientRequest(request: Request): Unit = request.request_type match {
    case "start" =>
      new Thread(game).start()
      send(JsonConverter.toJson(Map("message_type" -> "game_started", "message" -> "Game started !")))
    case "resume" =>
      game.resumeGame()
      send(JsonConverter.toJson(Map("message_type" -> "game_resumed", "message" -> "Game resumed !")))
    case "pause" =>
      game.pauseGame()
      send(JsonConverter.toJson(Map("message_type" -> "game_paused", "message" -> "Game paused !")))
    case "stop" =>
      game.stopGame()
      send(JsonConverter.toJson(Map("message_type" -> "game_stopped", "message" -> "Game stopped !")))
    case "build_tower" =>
      try {
        val r = request.request.convertTo[RequestBuild]
        game.buildTower(r)
      } catch {
        case _: Exception =>
          send(JsonConverter.toJson(Map("message_type" -> "error", "message" -> "Malformed JSON -> {`tower_type`: ..., `x`: ..., `y`: ...}")))
      }
    case "update_tower" =>
      try {
        val r = request.request.convertTo[RequestUpdate]
        game.updateTower(r)
      } catch {
        case _: Exception =>
          send(JsonConverter.toJson(Map("message_type" -> "error", "message" -> "Malformed JSON -> {`tower_type`: ..., `x`: ..., `y`: ...}")))
      }
    case "remove_tower" =>
      try {
        val r = request.request.convertTo[RequestDelete]
        game.removeTower(r)
      } catch {
        case _: Exception =>
          send(JsonConverter.toJson(Map("message_type" -> "error", "message" -> "Malformed JSON -> {`x`: ..., `y`: ...}")))
      }
    case _ =>
      send("Unknown request")
  }

  private def send(message: String): Unit = {
    out.println(message)
    out.flush()
  }

  override def run(): Unit = {
    try {
      for (line <- in.getLines()) {
        try {
          val json = line.parseJson
          clientRequest(json.convertTo[Request])
        } catch {
          case _: Exception =>
            send(JsonConverter.toJson(Map("message_type" -> "error", "message" -> "Malformed JSON -> {`request_type`: ..., `request`: {} }")))
        }
      }
    } catch {
      case _: SocketException =>
    }
    game.stopGame()
    println("Client excited !")
  }
}