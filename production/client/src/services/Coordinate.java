package services;

public class Coordinate {

    private final int mTileX;
    private final int mTileY;

    public Coordinate(int x, int y) {
        mTileX = x;
        mTileY = y;
    }

    public int getTileX() {
        return mTileX;
    }

    public int getTileY() {
        return mTileY;
    }

    public int getExactX() {
        return 0;
    }

    private int exactToTileValue(double value) {
        return ((int) ((value - 10) / 30)) * 2;
    }

    private int tileToExactValue(int value) {
        return ((value / 2) * 60) + 10;
    }

    private boolean equals(Coordinate that) {
        return mTileX == that.mTileX && mTileY == that.mTileY;
    }
}