package services;

public class Orientation {

    public static EOrientation getOrientation(int startX, int startY, int endX, int endY) {
        double angle = getAngle(startX, startY, endX, endY);
        if (angle >= 22.5 && angle < 67.5) {
            return EOrientation.SOUTH_EAST;
        } else if (angle >= 67.5 && angle < 110.5) {
            return EOrientation.SOUTH;
        } else if (angle >= 110.5 && angle < 155.5) {
            return EOrientation.SOUTH_WEST;
        } else if (angle >= 155.5 && angle < 200.5) {
            return EOrientation.WEST;
        } else if (angle >= 200.5 && angle < 245.5) {
            return EOrientation.NORTH_WEST;
        } else if (angle >= 245.5 && angle < 290.5) {
            return EOrientation.NORTH;
        } else if (angle >= 290.5 && angle < 335.5) {
            return EOrientation.NORTH_EAST;
        } else {
            return EOrientation.EAST;
        }
    }

    private static double getAngle(int startX, int startY, int endX, int endY) {
        double angle = Math.toDegrees(Math.atan2(endY - startY, endX - startX));
        if(angle < 0){
            angle += 360;
        }
        return angle;
    }
}
