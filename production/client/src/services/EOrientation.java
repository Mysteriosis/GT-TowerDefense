package services;

public enum EOrientation {
    NORTH("N"), NORTH_EAST("NE"), EAST("E"), SOUTH_EAST("SE"), SOUTH("S"), SOUTH_WEST("SW"), WEST("W"), NORTH_WEST("NW");

    private final String mValue;

    EOrientation(final String value) {
        mValue = value;
    }

    @Override
    public String toString() {
        return mValue;
    }
}
