import controllers.MenuController;
import controllers.MusicController;
import interfaces.IController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Launcher extends Application {

    private final String mHost = "127.0.0.1";
    private final int mPort = 1337;
    private IController mCurrent;

    @Override
    public void start(final Stage stage) throws Exception{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/menu.fxml"));
        Parent menu = loader.load();
        stage.setTitle("Tower Defense");
        stage.setScene(new Scene(menu));
        MenuController controller = loader.getController();
        controller.setHostAndPort(mHost, mPort, "");
        controller.setStage(stage);
        mCurrent = controller;
        stage.setResizable(false);
        stage.show();
        MusicController.getInstance().playMenu();
    }

    @Override
    public void stop(){
        System.out.println("Stage is closing");
        MusicController.getInstance().stop();
        if (mCurrent != null) {
            mCurrent.safeClose();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
