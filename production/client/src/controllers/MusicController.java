package controllers;

import javafx.animation.Transition;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;

import java.io.File;

public final class MusicController {

    private MediaPlayer mPlayer;

    private static MusicController mInstance;

    public static MusicController getInstance() {
        if (mInstance == null) {
            mInstance = new MusicController();
        }
        return mInstance;
    }

    public void stop() {
        if (mPlayer != null) {
            mPlayer.stop();
        }
    }

    public void playMenu() {
        String path = "src/resources/music/menu.wav";
        play(path);
    }

    public void playUnder100() {
        String path = "src/resources/music/under100.wav";
        play(path);
    }
    public void playUnder60() {
        String path = "src/resources/music/under60.wav";
        play(path);
    }

    public void playUnder20() {
        String path = "src/resources/music/under20.wav";
        play(path);
    }

    public void playBoss1() {
        String path = "src/resources/music/boss_1.wav";
        play(path);
    }

    public void playBoss2() {
        String path = "src/resources/music/boss_2.wav";
        play(path);
    }

    public void playFinalBoss() {
        String path = "src/resources/music/boss_3.wav";
        play(path);
    }

    private void play(String path) {
        if (mPlayer != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        for (int i = 0; i < 100; ++i) {
                            mPlayer.setVolume(mPlayer.getVolume() - 1);
                            Thread.sleep(2);
                        }
                        mPlayer.stop();
                        Media hit = new Media(new File(path).toURI().toString());
                        mPlayer = new MediaPlayer(hit);
                        mPlayer.setCycleCount(MediaPlayer.INDEFINITE);
                        mPlayer.play();
                        for (int i = 0; i < 100; ++i) {
                            mPlayer.setVolume(mPlayer.getVolume() + 1);
                            Thread.sleep(2);
                        }
                    } catch (InterruptedException e) {
                        System.out.println(e.getMessage());
                    }
                }
            }).start();
        } else {
            try {
                Media hit = new Media(new File(path).toURI().toString());
                mPlayer = new MediaPlayer(hit);
                mPlayer.setCycleCount(MediaPlayer.INDEFINITE);
                mPlayer.play();
                mPlayer.setVolume(100);
            } catch (Exception e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        }
    }
}
