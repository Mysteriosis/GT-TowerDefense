package controllers;

import javafx.scene.image.*;
import javafx.scene.input.KeyEvent;
import javafx.scene.text.Text;
import models.Coordinate;
import models.Monster;
import models.Tower;
import models.Projectile;
import services.EOrientation;
import services.Orientation;
import services.Toast;
import interfaces.IController;
import javafx.animation.PathTransition;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class SinglePlayerController implements IController {

    private PrintWriter mOut;
    private BufferedReader mIn;
    private String mDifficulty;
    private Thread thread;
    private boolean mIsRunning = false;
    private Stage mStage;
    private int[][] mGrid;
    private int clickedX = -1;
    private int clickedY = -1;
    private ImageView mGridView;
    private String mLives;
    private int mWaveNumber;
    private int mNumberOfProjectiles;
    private Map<Integer, Monster> monsters = new HashMap<>();
    private Coordinate[][] mCoordinateGrid;
    private Map<Coordinate, Tower> towers = new HashMap<>();
    private final LinkedList<JSONObject> mBuffer = new LinkedList<>();
    private KeyEvent mCurrentKeyEvent;
    private Image tile = new Image("resources/tile-64.png");
    private PixelReader tileReader = tile.getPixelReader();
    private ImageView focus = new ImageView(new Image("resources/selector.png"));
    private Runnable listener = new Runnable() {
        @Override
        public void run() {
            try {
                mIsRunning = true;
                String line;
                while (mIsRunning) {
                    line = mIn.readLine();
                    if (line == null) {
                        mIsRunning = false;
                        break;
                    }
                    if (mIsRunning && (!line.equals("\n") || !line.equals(""))) {
                        try {
                            line = line.replace("\n", "").replace("\r", "");
                            JSONObject response = new JSONObject(line);
                            synchronized (mBuffer) {
                                mBuffer.add(response);
                            }
                        } catch (JSONException e) {
                            System.out.println(e.getMessage());
                        }
                    }
                }
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }
    };

    private int tileToExactValue(int value) {
        return (value * 60);
    }

    private int exactToTileValue(double value) {
        return ((int) (value) / 60);
    }

    @Override
    public void setHostAndPort(final String host, final int port, final String gameType) throws IOException {
        Socket socket = new Socket(host, port);
        mOut = new PrintWriter(socket.getOutputStream(), true);
        mIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        initializeGame(gameType);
    }

    @Override
    public void setDifficulty(final String difficulty) {
        mDifficulty = difficulty;
    }

    private void initializeGame(String gameType) throws IOException {
        thread = new Thread(listener);
        thread.start();
        JSONObject message = new JSONObject();
        message.put("game_type", gameType);
        send(message);
    }

    private void send(JSONObject obj) throws IOException {
        mOut.println(obj.toString());
        mOut.flush();
    }

    private void initializeView(JSONObject object) throws JSONException {
        mGridView = new ImageView();
        Group tilemapGroup = new Group();
        JSONObject obj = object.getJSONObject("message");
        mLives = obj.getString("lives");
        lives.setText(obj.getString("lives"));
        money.setText(obj.getString("money"));
        JSONArray array = obj.getJSONArray("grid");
        mGrid = new int[array.length()][array.getJSONArray(0).length()];
        mCoordinateGrid = new Coordinate[array.length()][array.getJSONArray(0).length()];
        for (int i = 0; i < array.length(); ++i) {
            JSONArray sub = array.getJSONArray(i);
            for (int j = 0; j < sub.length(); ++j) {
                mGrid[j][i] = sub.getInt(j);
                mCoordinateGrid[j][i] = new Coordinate(j, i);
            }
        }
        mStage.getScene().setOnKeyTyped((keyEvent) -> {
            mCurrentKeyEvent = keyEvent;
            if (clickedX != -1 && clickedY != 1) {
                if (keyEvent.getCharacter().toLowerCase().equals("u")) {
                    Tower tower = towers.get(mCoordinateGrid[clickedX][clickedY]);
                    try {
                        JSONObject message = new JSONObject();
                        message.put("request_type", "update_tower");
                        JSONObject request = new JSONObject();
                        request.put("tower_type", tower.getTowerType());
                        request.put("x", clickedY);
                        request.put("y", clickedX);
                        message.put("request", request);
                        send(message);
                    } catch (IOException e) {
                        System.out.println(e.getMessage());
                    }
                } else if (keyEvent.getCharacter().toLowerCase().equals("r")) {
                    try {
                        JSONObject message = new JSONObject();
                        message.put("request_type", "remove_tower");
                        JSONObject request = new JSONObject();
                        request.put("x", clickedY);
                        request.put("y", clickedX);
                        message.put("request", request);
                        send(message);
                    } catch (IOException e) {
                        System.out.println(e.getMessage());
                    }
                }
            }
        });
        mGridView.setOnMouseClicked((mouseEvent) -> {
            int x = exactToTileValue(mouseEvent.getX());
            int y = exactToTileValue(mouseEvent.getY());
            if (mGrid[x][y] == 0 && mCurrentKeyEvent != null) {
                String c = mCurrentKeyEvent.getCharacter().toLowerCase();
                if (c.equals("5") || c.equals("4") || c.equals("3") || c.equals("2") || c.equals("1")) {
                    String tower = c.equals("5") ? "tower_41" : c.equals("4") ? "tower_31" : c.equals("3") ? "tower_21" : c.equals("2") ? "tower_11" : "tower_1";
                    try {
                        JSONObject message = new JSONObject();
                        message.put("request_type", "build_tower");
                        JSONObject request = new JSONObject();
                        request.put("tower_type", tower);
                        request.put("x", y);
                        request.put("y", x);
                        message.put("request", request);
                        send(message);
                    } catch (IOException e) {
                        System.out.println(e.getMessage());
                    }
                    clickedX = x;
                    clickedY = y;
                } else {
                    clickedX = -1;
                    clickedY = -1;
                    mCurrentKeyEvent = null;
                    focus.setVisible(false);
                }
            } else {
                // Do nothing, clicked on road !
                clickedX = -1;
                clickedY = -1;
                focus.setVisible(false);
            }
        });
        monsterLayer.getChildren().add(tilemapGroup);
        monsterLayer.getChildren().add(focus);
        tilemapGroup.getChildren().add(mGridView);
        monsterLayer.setPickOnBounds(false);
        stack.getChildren().add(monsterLayer);
        refreshGUI();
    }

    @FXML
    private StackPane stack;
    @FXML
    private Label lives;
    @FXML
    private Label money;
    @FXML
    private Group monsterLayer;

    public void initialize() {
        MusicController.getInstance().playUnder100();
        executeResponse();
    }

    private void executeResponse() {
        if (mBuffer.isEmpty()) {
            Task<Void> sleeper = new Task<Void>() {
                @Override
                protected Void call() throws Exception {
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        System.out.println(e.getMessage());
                    }
                    return null;
                }
            };
            sleeper.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
                @Override
                public void handle(WorkerStateEvent event) {
                    executeResponse();
                }
            });
            new Thread(sleeper).start();
        } else {
            JSONObject object;
            synchronized (mBuffer) {
                object = mBuffer.removeFirst();
            }
            try {
                parseResponse(object);
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }

    }

    private void parseResponse(JSONObject response) throws IOException {
        String key = "message";
        String toast;
        int id, x, y;
        JSONObject message;
        Monster m;
        String towerType;
        Tower tower;
        switch (response.getString("message_type")) {
            case "info":
                message = new JSONObject();
                message.put("request_type", "start");
                JSONObject difficulty = new JSONObject();
                difficulty.put("difficulty", mDifficulty);
                message.put("request", difficulty);
                send(message);
                break;
            case "initialization":
                /* OK */
                initializeView(response);
                break;
            case "wave_started":
                waves.setText(response.getString(key).replace("wave_", ""));
                mWaveNumber = Integer.parseInt(response.getString(key).replace("wave_", ""));
                switch(mWaveNumber) {
                    case 10:
                        MusicController.getInstance().playBoss1();
                        break;
                    case 11:
                    case 21:
                        if (Integer.parseInt(mLives) > 60) {
                            MusicController.getInstance().playUnder100();
                        } else if (Integer.parseInt(mLives) > 20) {
                            MusicController.getInstance().playUnder60();
                        } else {
                            MusicController.getInstance().playUnder20();
                        }
                        break;
                    case 20:
                        MusicController.getInstance().playBoss2();
                        break;
                    case 30:
                        MusicController.getInstance().playFinalBoss();
                        break;
                    default:
                        break;
                }
                break;
            case "wave_ended":
            case "game_started":
            case "game_stopped":
            case "game_paused":
            case "game_resumed":
                break;
            case "game_lost":
                toast = "Game over !";
                Toast.makeText(mStage, toast, 3000, 250, 250);
                break;
            case "game_won":
                toast = "Well done !";
                Toast.makeText(mStage, toast, 3000, 250, 250);
                break;
            case "monster_create":
                JSONObject monster = response.getJSONObject(key);
                id = monster.getInt("id");
                x = monster.getInt("y");
                y = monster.getInt("x");
                String monsterType = monster.getString("monster_type");
                m = new Monster(id, monsterType, x, y);
                monsters.put(id, m);
                monsterLayer.getChildren().add(m);
                break;
            case "monster_move":
                JSONArray moves = response.getJSONArray(key);
                for (int i = 0; i < moves.length(); ++i) {
                    message = moves.getJSONObject(i);
                    id = message.getInt("id");
                    final int next_x = message.getInt("y");
                    final int next_y = message.getInt("x");
                    m = monsters.get(id);
                    if (m != null) {
                        m.updateLocation(next_x, next_y);
                        if (!m.isOver()) {
                            EOrientation o = Orientation.getOrientation(m.getLastX(), m.getLastY(), m.getNextX(), m.getNextY());
                            m.setOrientation(o);
                            Path monsterPath;
                            PathTransition animation;
                            monsterPath = new Path(new MoveTo(tileToExactValue(m.getLastX()) + 30, tileToExactValue(m.getLastY()) + 30));
                            monsterPath.getElements().add(new LineTo(tileToExactValue(m.getNextX()) + 30, tileToExactValue(m.getNextY()) + 30));
                            animation = new PathTransition(Duration.millis(next_y == -1 ? 550 : 550), monsterPath, m);
                            animation.setOnFinished(new EventHandler<ActionEvent>() {
                                @Override
                                public void handle(ActionEvent actionEvent) {
                                    PathTransition finishedAnimation = (PathTransition) actionEvent.getSource();
                                    Monster finishedMonster = (Monster) finishedAnimation.getNode();
                                    finishedMonster.setAnimationRunning(false);
                                }
                            });
                            m.setAnimationRunning(true);
                            animation.play();
                        } else if (!m.isAnimationRunning() && m.isOver()) {
                            removeMonster(m);
                        }
                        refreshGUI();
                    }
                }
                break;
            case "monster_hit":
                /* OK */
                JSONArray array = response.getJSONArray(key);
                Projectile projectile;
                Path projectilePath;
                PathTransition animation;
                for (int i = 0; i < array.length(); ++i) {
                    JSONObject obj = array.getJSONObject(i);
                    x = obj.getJSONObject("tower").getInt("y");
                    y = obj.getJSONObject("tower").getInt("x");
                    int towerX = tileToExactValue(x) + 30;
                    int towerY = tileToExactValue(y) + 30;
                    int monsterX = tileToExactValue(obj.getJSONObject("monster").getInt("y")) + 30;
                    int monsterY = tileToExactValue(obj.getJSONObject("monster").getInt("x")) + 30;
                    int monsterId = obj.getJSONObject("monster").getInt("id");
                    if (monsters.containsKey(monsterId)) {
                        m = monsters.get(monsterId);
                        int modulus;
                        switch (m.getMonsterName()) {
                            case "monster_1":
                            case "monster_2":
                            case "monster_3":
                            case "monster_4":
                                modulus = 1;
                                break;
                            case "monster_5":
                                modulus = 2;
                                break;
                            case "monster_6":
                                modulus = 3;
                                break;
                            case "monster_7":
                                modulus = 5;
                                break;
                            case "monster_8":
                                modulus = 7;
                                break;
                            case "monster_9":
                            case "boss_1":
                                modulus = 10;
                                break;
                            case "boss_2":
                                modulus = 25;
                                break;
                            case "boss_3":
                                modulus = 50;
                                break;
                            default:
                                System.out.println("Unknown monster !!!!");
                                modulus = 1;
                        }
                        EOrientation o = Orientation.getOrientation(towerX, towerY, monsterX, monsterY);
                        tower = towers.get(mCoordinateGrid[x][y]);
                        tower.setOrientation(o);
                        tower.toFront();
                        if (mNumberOfProjectiles % modulus == 0) {
                            int lastX = tileToExactValue(m.getLastX()) + 30;
                            int lastY = tileToExactValue(m.getLastY()) + 30;
                            int nextX = tileToExactValue(m.getNextX()) + 30;
                            int nextY = tileToExactValue(m.getNextY()) + 30;
                            final int monster_x = (lastX + nextX) / 2;
                            final int monster_y = (lastY + nextY) / 2;
                            final Monster mstr = monsters.get(monsterId);
                            projectile = new Projectile(monster_x,monster_y, towerX, towerY, new Color(0.5, 0.2, 0.5, 1));
                            projectilePath = new Path(new MoveTo(projectile.getStartX(), projectile.getStartY()));
                            projectilePath.getElements().add(new LineTo(projectile.getEndX(), projectile.getEndY()));
                            animation = new PathTransition(Duration.millis(200), projectilePath, projectile);

                            animation.setOnFinished(new EventHandler<ActionEvent>() {
                                @Override
                                public void handle(ActionEvent actionEvent) {
                                    PathTransition finishedAnimation = (PathTransition) actionEvent.getSource();
                                    Projectile finishedProjectile = (Projectile) finishedAnimation.getNode();
                                    finishedProjectile.setVisible(false);
                                    mstr.setUnderFire(false);
                                    if (monsterLayer.getChildren().contains(finishedProjectile)) {
                                        monsterLayer.getChildren().remove(finishedProjectile);
                                    }
                                }
                            });
                            mstr.setUnderFire(true);
                            monsterLayer.getChildren().add(projectile);
                            tower.toFront();
                            projectile.setVisible(true);
                            animation.play();
                        } else {
                            m.setUnderFire(false);
                        }
                        mNumberOfProjectiles += 1;
                    }
                }
                break;
            case "monster_death":
                /* OK */
                id = response.getJSONObject(key).getInt("id");
                removeMonster(monsters.get(id));
                refreshGUI();
                break;
            case "tower_build":
                /* OK */
                message = response.getJSONObject(key);
                x = message.getInt("y");
                y = message.getInt("x");
                final int towerX = x;
                final int towerY = y;
                towerType = message.getString("tower_type");
                tower = new Tower(towerType, x, y);
                final Tower t = tower;
                tower.setOnMouseClicked((mouseEvent) -> {
                    clickedX = towerX;
                    clickedY = towerY;
                    focus.setX(tileToExactValue(towerX));
                    focus.setY(tileToExactValue(towerY));
                    focus.setVisible(true);
                });

                towers.put(mCoordinateGrid[x][y], tower);
                monsterLayer.getChildren().add(tower);
                focus.setX(tileToExactValue(towerX));
                focus.setY(tileToExactValue(towerY));
                focus.setVisible(true);
                clickedX = towerX;
                clickedY = towerY;
                refreshGUI();
                break;
            case "tower_update":
                /* OK */
                message = response.getJSONObject(key);
                x = message.getInt("y");
                y = message.getInt("x");
                towerType = message.getString("tower_type");
                tower = towers.get(mCoordinateGrid[x][y]);
                tower.setTowerType(towerType);
                focus.setX(tileToExactValue(x));
                focus.setY(tileToExactValue(y));
                focus.setVisible(true);
                break;
            case "tower_delete":
                /* OK */
                message = response.getJSONObject(key);
                x = message.getInt("y");
                y = message.getInt("x");
                tower = towers.remove(mCoordinateGrid[x][y]);
                focus.setVisible(false);
                monsterLayer.getChildren().remove(tower);
                refreshGUI();
                break;
            case "money":
                /* OK */
                money.setText("" + response.get(key));
                break;
            case "lives":
                /* OK */
                if (Integer.decode(response.getString(key)) == 60) {
                    MusicController.getInstance().playUnder60();
                } else if (Integer.decode(response.getString(key)) == 20) {
                    MusicController.getInstance().playUnder20();
                }
                lives.setText(response.getString(key));
                mLives = response.getString(key);
                break;
            case "error":
                /* OK */
                break;
            default:
                /* OK */
                System.out.println("Unknown type : " + response.getString("message_type"));
        }
        executeResponse();
    }

    private void removeMonster(Monster m) {
        if (m.isAnimationRunning()) {
            Task<Void> remover = new Task<Void>() {
                @Override
                protected Void call() throws Exception {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        System.out.println(e.getMessage());
                    }
                    return null;
                }
            };
            remover.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
                @Override
                public void handle(WorkerStateEvent event) {
                    removeMonster(m);
                }
            });
            new Thread(remover).start();
        } else {
            m.setVisible(false);
            monsters.remove(m.getMonsterId());
        }
    }

    private void refreshGUI() {
        byte[] buffer = new byte[64 * 64 * 4];
        WritablePixelFormat<ByteBuffer> picFormat = WritablePixelFormat.getByteBgraInstance();

        WritableImage paintedMap = new WritableImage(900, 900);
        PixelWriter tileWriter = paintedMap.getPixelWriter();
        for (int x = 0; x < 15; ++x) {
            for (int y = 0; y < 15; ++y) {
                switch (mGrid[x][y]) {
                    case 0:
                        tileReader.getPixels(384, 64, 64, 64, picFormat, buffer, 0, 256);
                        break;
                    case 1:
                        tileReader.getPixels(384, 192, 64, 64, picFormat, buffer, 0, 256);
                        break;
                    case 2:
                        tileReader.getPixels(448, 128, 60, 64, picFormat, buffer, 0, 256);
                        break;
                    case 3:
                        tileReader.getPixels(256, 192, 60, 60, picFormat, buffer, 0, 256);
                        break;
                    case 4:
                        tileReader.getPixels(192, 192, 60, 60, picFormat, buffer, 0, 256);
                        break;
                    case 5:
                        tileReader.getPixels(192, 128, 60, 60, picFormat, buffer, 0, 256);
                        break;
                    case 6:
                        tileReader.getPixels(256, 128, 60, 60, picFormat, buffer, 0, 256);
                        break;
                    case 7:
                        tileReader.getPixels(451, 451, 60, 60, picFormat, buffer, 0, 256);
                        break;
                    case 8:
                        tileReader.getPixels(0, 384, 60, 60, picFormat, buffer, 0, 256);
                        break;
                    case 9:
                        tileReader.getPixels(259, 451, 60, 60, picFormat, buffer, 0, 256);
                        break;
                    case 10:
                        tileReader.getPixels(130, 384, 60, 60, picFormat, buffer, 0, 256);
                        break;
                    case 11:
                        tileReader.getPixels(448, 192, 60, 60, picFormat, buffer, 0, 256);
                        break;
                }
                tileWriter.setPixels(x * 60, y * 60, 60, 60, picFormat, buffer, 0, 256);
            }
        }
        mGridView.setImage(paintedMap);
    }

    @FXML Text waves;
    @Override
    public void setStage(final Stage stage) {
        mStage = stage;
    }

    @Override
    public void safeClose() {
        System.out.println("Safely closed !");
        mIsRunning = false;
    }
}