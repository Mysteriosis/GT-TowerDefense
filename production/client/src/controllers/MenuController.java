package controllers;

import interfaces.IController;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.stage.Screen;
import javafx.stage.Stage;

import java.io.IOException;

public class MenuController implements IController {

    private String mHost;
    private int mPort;
    private IController mCurrent;
    private String mGameType;
    private String mDifficulty;
    @Override
    public void setHostAndPort(final String host, final int port, final String gameType) {
        mHost = host;
        mPort = port;
        mGameType = "single";
    }

    private Stage mStage;
    @Override
    public void setStage(final Stage stage) {
        mStage = stage;
    }

    @Override
    public void setDifficulty(final String difficulty) {

    }

    @FXML
    RadioButton easy_mode;
    @FXML RadioButton medium_mode;
    @FXML RadioButton hard_mode;
    public void initialize() {
        easy_mode.setSelected(true);
        mDifficulty = "easy";
        easy_mode.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                easy_mode.setSelected(true);
                medium_mode.setSelected(false);
                hard_mode.setSelected(false);
                mDifficulty = "easy";
            }
        });
        medium_mode.setSelected(false);
        medium_mode.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                easy_mode.setSelected(false);
                medium_mode.setSelected(true);
                hard_mode.setSelected(false);
                mDifficulty = "medium";
            }
        });
        hard_mode.setSelected(false);
        hard_mode.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                easy_mode.setSelected(false);
                medium_mode.setSelected(false);
                hard_mode.setSelected(true);
                mDifficulty = "hard";
            }
        });
    }

    @FXML private void singlePlayer() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/singleplayer.fxml"));
        Parent singleplayer = loader.load();
        mStage.setScene(new Scene(singleplayer));
        SinglePlayerController controller = loader.getController();
        controller.setHostAndPort(mHost, mPort, mGameType);
        controller.setStage(mStage);
        controller.setDifficulty(mDifficulty);
        mCurrent = controller;
        mStage.setResizable(false);
        Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
        mStage.setX((primScreenBounds.getWidth() - mStage.getWidth()) / 2);
        mStage.setY((primScreenBounds.getHeight() - mStage.getHeight()) / 2);
        mStage.show();
    }

    @FXML private void coopMode() throws IOException {
        mGameType = "coop";
        singlePlayer();
    }

    @Override
    public void safeClose() {
        if (mCurrent != null) {
            mCurrent.safeClose();
        }
    }
}
