package interfaces;

import javafx.stage.Stage;

import java.io.IOException;

public interface IController {

    void setHostAndPort(final String host, final int port, final String gameType) throws IOException;

    void safeClose();

    void setDifficulty(final String difficulty);

    void setStage(final Stage stage);
}
