package models;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import services.EOrientation;

public class Tower extends ImageView {

    private String mTowerType;
    private EOrientation mOrientation;

    public Tower(String towerType, int x, int y) {
        mTowerType = towerType;
        setX(tileToExactValue(x));
        setY(tileToExactValue(y));
        mOrientation = EOrientation.SOUTH;
        setTowerImage();
    }

    public String getTowerType() {
        return mTowerType;
    }

    public void setTowerType(String towerType) {
        mTowerType = towerType;
        setTowerImage();
    }

    private int tileToExactValue(int value) {
        return (value * 60);
    }

    private void setTowerImage() {
        Image image;
        switch (mTowerType) {
            case "tower_1":
                image = new Image("resources/towers/tower_1_" + mOrientation + ".png");
                break;
            case "tower_2":
                image = new Image("resources/towers/tower_2_" + mOrientation + ".png");
                break;
            case "tower_3":
                image = new Image("resources/towers/tower_3_" + mOrientation + ".png");
                break;
            case "tower_11":
                image = new Image("resources/towers/tower_11_" + mOrientation + ".png");
                break;
            case "tower_12":
                image = new Image("resources/towers/tower_12_" + mOrientation + ".png");
                break;
            case "tower_13":
                image = new Image("resources/towers/tower_13_" + mOrientation + ".png");
                break;
            case "tower_21":
                image = new Image("resources/towers/tower_21_" + mOrientation + ".png");
                break;
            case "tower_22":
                image = new Image("resources/towers/tower_22_" + mOrientation + ".png");
                break;
            case "tower_23":
                image = new Image("resources/towers/tower_23_" + mOrientation + ".png");
                break;
            case "tower_31":
                image = new Image("resources/towers/tower_31_" + mOrientation + ".png");
                break;
            case "tower_32":
                image = new Image("resources/towers/tower_32_" + mOrientation + ".png");
                break;
            case "tower_33":
                image = new Image("resources/towers/tower_33_" + mOrientation + ".png");
                break;
            case "tower_41":
                image = new Image("resources/towers/tower_41_" + mOrientation + ".png");
                break;
            case "tower_42":
                image = new Image("resources/towers/tower_42_" + mOrientation + ".png");
                break;
            case "tower_43":
                image = new Image("resources/towers/tower_43_" + mOrientation + ".png");
                break;
            default:
                image = new Image("resources/towers/tower_1_" + mOrientation + ".png");
                break;
        }
        setImage(image);
    }

    public void setOrientation(EOrientation orientation) {
        mOrientation = orientation;
        setTowerImage();
    }
}