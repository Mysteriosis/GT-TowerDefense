package models;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import services.EOrientation;

public class Monster extends ImageView {

    private int id;
    private int nextX;
    private int nextY;
    private int lastX;
    private int lastY;
    private final String mMonsterId;
    private EOrientation mOrientation;
    private boolean isAnimationRunning;
    private boolean isUnderFire;


    public Monster(int id, String monsterId, int tileX, int tileY) {
        this.id = id;
        nextX = tileX;
        nextY = tileY;
        lastX = nextX;
        lastY = nextY;
        mMonsterId = monsterId;
        setX(tileToExactValue(tileX));
        setY(tileToExactValue(tileY));
        mOrientation = EOrientation.NORTH;
        setMonsterImage();
    }

    public int getMonsterId() {
        return id;
    }
    public String getMonsterName() { return mMonsterId; }

    public int getNextX() { return nextX; }
    public int getNextY() { return nextY; }
    public int getLastX() { return lastX; }
    public int getLastY() { return lastY; }

    public void updateLocation(int x, int y) {
        lastX = nextX;
        lastY = nextY;
        nextX = x;
        nextY = y;
        setX(tileToExactValue(nextX));
        setY(tileToExactValue(nextY));
    }

    public void setOrientation(EOrientation orientation) {
        mOrientation = orientation;
        setMonsterImage();
    }

    public void setAnimationRunning(boolean value) {
        isAnimationRunning = value;
    }

    public void setUnderFire(boolean value) {
        isUnderFire = value;
    }

    public boolean isAnimationRunning() {
        return isAnimationRunning || isUnderFire;
    }

    private void setMonsterImage() {
        Image image;
        switch (mMonsterId) {
            case "monster_1":
                image = new Image("resources/monsters/monster_1_" + mOrientation + ".png");
                break;
            case "monster_2":
                image = new Image("resources/monsters/monster_2_" + mOrientation + ".png");
                break;
            case "monster_3":
                image = new Image("resources/monsters/monster_3_" + mOrientation + ".png");
                break;
            case "monster_4":
                image = new Image("resources/monsters/monster_4_" + mOrientation + ".png");
                break;
            case "monster_5":
                image = new Image("resources/monsters/monster_5_" + mOrientation + ".png");
                break;
            case "monster_6":
                image = new Image("resources/monsters/monster_6_" + mOrientation + ".png");
                break;
            case "monster_7":
                image = new Image("resources/monsters/monster_7_" + mOrientation + ".png");
                break;
            case "monster_8":
                image = new Image("resources/monsters/monster_8_" + mOrientation + ".png");
                break;
            case "monster_9":
                image = new Image("resources/monsters/monster_9_" + mOrientation + ".png");
                break;
            case "boss_1":
                image = new Image("resources/monsters/boss_1_" + mOrientation + ".png");
                break;
            case "boss_2":
                image = new Image("resources/monsters/boss_2_" + mOrientation + ".png");
                break;
            case "boss_3":
                image = new Image("resources/monsters/boss_3_" + mOrientation + ".png");
                break;
            default:
                image = new Image("resources/monsters/monster_1_" + mOrientation + ".png");
                break;
        }
        setImage(image);
    }

    public boolean isOver() {
        return nextY == -1 || nextX == -1;
    }

    private int tileToExactValue(int value) {
        return (value * 60);
    }

}
