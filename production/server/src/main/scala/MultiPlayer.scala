import java.io.PrintStream
import java.net.SocketException

import engines.PlayerEngine
import models.JsonConverter
import models.JsonImplicit._
import models.Model._
import spray.json._

import scala.io.BufferedSource

case class MultiPlayer(in: BufferedSource, out: PrintStream) extends PlayerEngine(in, out) {

  var in2: BufferedSource = _
  var out2: PrintStream = _
  var player2: Thread = _
  var counter = 0

  def setSecondPlayer(in: BufferedSource, out: PrintStream) {
    in2 = in
    out2 = out
    player2 = new Thread(new Runnable {
      override def run(): Unit = {
        try {
          for (line <- in2.getLines()) {
            try {
              val json = line.parseJson
              clientRequest(json.convertTo[Request])
            } catch {
              case _: Exception =>
                send(JsonConverter.toJson(Map("message_type" -> "error", "message" -> "Malformed JSON -> {`request_type`: ..., `request`: {} }")))
            }
          }
        } catch {
          case _: SocketException =>
        }
        println("Client 2 excited !")
      }
    })
    player2.start()
  }

  override def clientRequest(request: Request): Unit = request.request_type match {
    case "start" =>
      counter += 1
      if (counter == 2) {
        val difficulty = request.request.convertTo[Difficulty].difficulty
        mGame.setDifficulty(difficulty)
        new Thread(mGame).start()
        send(JsonConverter.toJson(Map("message_type" -> "game_started", "message" -> "Game started !")))
      }
    case _ => super.clientRequest(request)
  }

  override def send(message: String): Unit = {
    out.println(message)
    out2.println(message)
    out.flush()
    out2.flush()
  }
}