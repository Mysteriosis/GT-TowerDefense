import java.io.PrintStream

import engines.PlayerEngine
import models.JsonConverter
import models.Model._
import models.JsonImplicit._

import scala.io.BufferedSource

case class SinglePlayer(in: BufferedSource, out: PrintStream) extends PlayerEngine(in, out) {

  override def clientRequest(request: Request): Unit = request.request_type match {
    case "start" =>
      val difficulty = request.request.convertTo[Difficulty].difficulty
      mGame.setDifficulty(difficulty)
      new Thread(mGame).start()
      send(JsonConverter.toJson(Map("message_type" -> "game_started", "message" -> "Game started !")))
    case _ => super.clientRequest(request)
  }

  override def send(message: String): Unit = {
    out.println(message)
    out.flush()
  }
}