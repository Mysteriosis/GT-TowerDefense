import java.io.PrintStream
import java.net.ServerSocket

import models.JsonConverter

import scala.io.BufferedSource
import spray.json._
import models.JsonImplicit._
import models.Model.GameType

object Launcher {

  val port = 1337
  var isMulti = false
  var multi: MultiPlayer = _

  private def listener() = {
    val server = new ServerSocket(port)
    println("Server started...")
    println("Waiting for incoming connexions...")
    while (true) {
      val socket = server.accept()
      val in = new BufferedSource(socket.getInputStream)
      val out = new PrintStream(socket.getOutputStream)
      try {
        in.getLines().next().parseJson.convertTo[GameType].game_type match {
          case "single" =>
            val message = JsonConverter.toJson(Map("message_type" -> "info", "message" -> "player_0"))
            out.println(message)
            out.flush()
            SinglePlayer(in, out).start()
          case "coop" =>
            println("Coop mode !")
            if (isMulti) {
              isMulti = false
              multi.setSecondPlayer(in, out)
              out.println(JsonConverter.toJson(Map("message_type" -> "info", "message" -> "player_2")))
              out.flush()
            } else {
              isMulti = true
              val message = JsonConverter.toJson(Map("message_type" -> "info", "message" -> "player_1"))
              out.println(message)
              out.flush()
              multi = MultiPlayer(in, out)
              multi.start()
            }
          case "1v1" => println("1v1 mode !")
          case _ => throw new Exception
        }
      } catch {
        case e: Exception =>
          e.printStackTrace()
          out.println("Invalid game type !")
          out.flush()
          socket.close()
      }
    }
  }

  def main(args: Array[String]): Unit = {
    listener()
  }
}