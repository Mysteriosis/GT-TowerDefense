package models

import spray.json.JsValue

object Model {

  case class Tower(id: String, cost: Int, power: Int, speed: Int, range: Int, next_upgrade: Option[String], previous: Option[String]) {
    override def toString: String =
    "{ \"id\":\"" + id + "\", \"cost\":" + cost + ", \"speed\":" + speed + ", \"range\":" + range + " }"
  }

  case class Grid(start_x: Int, start_y: Int, end_x: Int, end_y: Int, grid: Array[Array[Int]])
  case class GridDifficulty(easy: List[Grid], medium: List[Grid], hard: List[Grid])

  case class Monster(id: String, health: Int, speed: Int, money: Int, damage: Int)

  case class MonsterInWave(id: String, number: Int)

  case class Wave(id: String, monsters: List[MonsterInWave])

  case class GameType(game_type: String)

  case class Request(request_type: String, request: JsValue)

  case class Difficulty(difficulty: String)

  case class RequestBuild(tower_type: String, x: Int, y: Int) {
    override def toString: String =
    "{\"tower_type\":\"" + tower_type + "\", \"x\": " + x + ", \"y\": " + y + "}"
  }

  case class RequestUpdate(var tower_type: String, x: Int, y: Int) {
    override def toString: String =
      "{\"tower_type\":\"" + tower_type + "\", \"x\": " + x + ", \"y\": " + y + "}"
  }

  case class RequestDelete(x: Int, y: Int) {
    override def toString: String =
      "{\"x\": " + x + ", \"y\": " + y + "}"
  }

  case class MonsterInGame(id: Int, monsterType: String,  var last_x: Int, var last_y: Int, var x: Int, var y: Int, var currentHealth: Int, health: Int, speed: Int, money: Int, damage: Int, var current_move: Int, var path: List[(Int, Int)]) {
    override def toString: String =
      "{\"id\":" + id + ", \"monster_type\": \"" + monsterType + "\", \"last_x\": " + last_x + ", \"last_y\": " + last_y + ", \"x\": " + x + ", \"y\": " + y + ", \"current_health\": " + currentHealth + ", \"health\": " + health + ", \"speed\": " + speed + ", \"damage\": " + damage + "}"
  }
}