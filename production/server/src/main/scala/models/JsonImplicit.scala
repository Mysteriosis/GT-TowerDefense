package models

import spray.json.{DefaultJsonProtocol, JsFalse, JsNumber, JsString, JsTrue, JsValue, JsonFormat, RootJsonFormat}
import models.Model._

import scala.util.Try



object JsonImplicit extends DefaultJsonProtocol {
  implicit val towersFormat: RootJsonFormat[Tower] = jsonFormat7(Tower)
  implicit val gridsFormat: RootJsonFormat[Grid] = jsonFormat5(Grid)
  implicit val monstersFormat: RootJsonFormat[Monster] = jsonFormat5(Monster)
  implicit val monstersInWaveFormat: RootJsonFormat[MonsterInWave] = jsonFormat2(MonsterInWave)
  implicit val wavesFormat: RootJsonFormat[Wave] = jsonFormat2(Wave)
  implicit val gameTypeFormat: RootJsonFormat[GameType] = jsonFormat1(GameType)
  implicit val requestFormat: RootJsonFormat[Request] = jsonFormat2(Request)
  implicit val monsterInGameFormat: RootJsonFormat[MonsterInGame] = jsonFormat13(MonsterInGame)
  implicit val requestBuildFormat: RootJsonFormat[RequestBuild] = jsonFormat3(RequestBuild)
  implicit val requestUpdateFormat: RootJsonFormat[RequestUpdate] = jsonFormat3(RequestUpdate)
  implicit val requestDeleteFormat: RootJsonFormat[RequestDelete] = jsonFormat2(RequestDelete)
  implicit val difficultyFormat: RootJsonFormat[Difficulty] = jsonFormat1(Difficulty)
  implicit val gridDifficultyFormat: RootJsonFormat[GridDifficulty] = jsonFormat3(GridDifficulty)
  implicit object AnyJsonFormat extends JsonFormat[Any] {
    def write(x: Any): JsValue = x match {
      case n: Int => JsNumber(n)
      case s: String => JsString(s)
      case b: Boolean if b => JsTrue
      case b: Boolean if !b => JsFalse
    }
    def read(value: JsValue): Any = value match {
      case JsNumber(n) => n.intValue()
      case JsString(s) => s
      case JsTrue => true
      case JsFalse => false
    }
  }
  implicit class JsFieldOps(val field: Option[JsValue]) {
    def /(name: String) = field map (_ / name) getOrElse this
    def ===(x: JsValue) = field.contains(x)
    def =!=(x: JsValue) = !field.contains(x)
  }
  implicit class JsValueOps(val value: JsValue) extends AnyVal {
    def /(name: String) = JsFieldOps(Try(value.asJsObject).toOption.flatMap(_.fields.get(name)))
  }
}
