package engines

import models.JsonConverter
import models.JsonImplicit._
import models.Model._
import spray.json._
import utils.PathSolver

import scala.collection.mutable.ListBuffer
import scala.util.Random
import scala.util.control.Breaks.{break, breakable}

case class GameEngineNormal(controller: PlayerEngine) extends Runnable {

  var difficulty = ""

  def setDifficulty(diff: String): Unit = {
    difficulty = diff
  }

  val towers: Map[String, Tower] = scala.io.Source.fromResource("towers.json").mkString.parseJson.convertTo[List[Tower]].map(t => t.id -> t).toMap
  val monsters: Map[String, Monster] = scala.io.Source.fromResource("monsters.json").mkString.parseJson.convertTo[List[Monster]].map(m => m.id -> m).toMap
  val waves: Map[String, Wave] = scala.io.Source.fromResource("waves.json").mkString.parseJson.convertTo[List[Wave]].map(w => w.id -> w).toMap
  val orders: List[String] = scala.io.Source.fromResource("orders.json").mkString.parseJson.convertTo[List[String]]
  val grid_difficulty: GridDifficulty = scala.io.Source.fromResource("grid.json").mkString.parseJson.convertTo[GridDifficulty]
  val initial_money: Int = 500
  val initial_lives: Int = 100
  var grid_object: Grid = Grid(0, 0, 0, 0, Array())
  var grid: Array[Array[Int]] = Array()
  var start_x: Int = 0
  var start_y: Int = 0
  var end_x: Int = 0
  var end_y: Int = 0
  private var isRunning = true
  private var isPaused = false
  private var monsterId = 0
  private var money = initial_money
  private var lives = initial_lives
  private val monstersList: ListBuffer[MonsterInGame] = ListBuffer()
  private val towersMap: scala.collection.mutable.Map[(Int, Int), Tower] = scala.collection.mutable.Map()

  private def generatePath(): List[(Int, Int)] = PathSolver(grid, start_x, start_y, end_x, end_y).solve()

  override def run(): Unit = {
    difficulty match {
      case "easy" => grid_object = Random.shuffle(grid_difficulty.easy).head
      case "medium" => grid_object = Random.shuffle(grid_difficulty.medium).head
      case "hard" => grid_object = Random.shuffle(grid_difficulty.hard).head
      case _ => grid_object = Random.shuffle(grid_difficulty.easy).head
    }
    grid = grid_object.grid
    start_x = grid_object.start_x
    start_y = grid_object.start_y
    end_x = grid_object.end_x
    end_y = grid_object.end_y
    breakable {
      val initialization = Map("money" -> initial_money.toString, "lives" -> initial_lives.toString, "grid" -> grid)
      controller.send(JsonConverter.toJson(Map("message_type" -> "initialization", "message" -> initialization)))
      Thread.sleep(5000)
      for (order <- orders) {
        if (!isRunning) break
        checkPause()
        controller.send(JsonConverter.toJson(Map("message_type" -> "wave_started", "message" -> order)))
        waves.get(order) match {
          case Some(wave) =>
            checkPause()
            for (monstersInWave <- wave.monsters) {
              checkPause()
              for (_ <- 0.until(monstersInWave.number)) {
                checkPause()
                monsters.get(monstersInWave.id) match {
                  case Some(m) =>
                    checkPause()
                    val _m = MonsterInGame(monsterId, monstersInWave.id, start_x, start_y, start_x, start_y, m.health, m.health, m.speed, m.money, m.damage, 0, generatePath())
                    controller.send("{\"message_type\": \"monster_create\", \"message\": " + _m + "}")
                    monsterId += 1
                    monstersList.append(_m)
                    Thread.sleep(500)
                    moveMonsters()
                    fireTowers()
                    if (lives <= 0) {
                      gameLost()
                      break
                    }
                  case _ => break
                }
              }
            }
            while (monstersList.nonEmpty) {
              Thread.sleep(500)
              moveMonsters()
              fireTowers()
              if (lives <= 0) {
                gameLost()
                break
              }
              if (!isRunning) {
                break
              }
            }
          case _ => break
        }
        controller.send(JsonConverter.toJson(Map("message_type" -> "wave_ended", "message" -> order)))
        Thread.sleep(5000)
      }
      gameWon()
    }
    println("Game ended !")
  }

  def stopGame(): Unit = {
    isPaused = false
    isRunning = false
  }
  def pauseGame(): Unit = isPaused = true
  def resumeGame(): Unit = isPaused = false

  def buildTower(r: RequestBuild): Unit = {
    val tower = towers.get(r.tower_type)
    tower match {
      case Some(t) =>
        if (r.x >= grid.length || r.y >= grid(r.x).length) {
          controller.send(JsonConverter.toJson(Map("message_type" -> "error", "message" -> "X or Y coordinates out of bound !")))
          return
        }
        if (grid(r.x)(r.y) != 0) {
          controller.send(JsonConverter.toJson(Map("message_type" -> "error", "message" -> "Cell not available !")))
          return
        }
        if (money < t.cost) {
          controller.send(JsonConverter.toJson(Map("message_type" -> "error", "message" -> "Not enough money !")))
          return
        }
        money -= t.cost
        controller.send(JsonConverter.toJson(Map("message_type" -> "money", "message" -> money)))
        grid(r.x)(r.y) = 20
        controller.send("{\"message_type\":\"tower_build\",\"message\":" + r + "}")
        towersMap += (r.x, r.y) -> t
      case _ => controller.send(JsonConverter.toJson(Map("message_type" -> "error", "message" -> "Tower type does not exist !")))
    }
  }

  def updateTower(r: RequestUpdate): Unit = {
    val tower = towers.get(r.tower_type)
    tower match {
      case Some(t) =>
        if (r.x >= grid.length || r.y >= grid(r.x).length) {
          controller.send(JsonConverter.toJson(Map("message_type" -> "error", "message" -> "X or Y coordinates out of bound !")))
          return
        }
        if (grid(r.x)(r.y) != 20) {
          controller.send(JsonConverter.toJson(Map("message_type" -> "error", "message" -> "No tower to upgrade")))
          return
        }
        t.next_upgrade match {
          case Some(nextTower) =>
            towers.get(nextTower) match {
              case Some(_t) =>
                if (money < _t.cost) {
                  controller.send(JsonConverter.toJson(Map("message_type" -> "error", "message" -> "Not enough money !")))
                  return
                }
                money -= _t.cost
                controller.send(JsonConverter.toJson(Map("message_type" -> "money", "message" -> money)))
                r.tower_type = nextTower
                controller.send("{\"message_type\":\"tower_update\",\"message\":" + r + "}")
                towersMap += (r.x, r.y) -> _t
              case _ => // Should never happen !
            }
          case _ => controller.send(JsonConverter.toJson(Map("message_type" -> "error", "message" -> "No upgrade available")))
        }
      case _ => controller.send(JsonConverter.toJson(Map("message_type" -> "error", "message" -> "Tower type does not exist !")))
    }
  }

  def removeTower(r: RequestDelete): Unit = {
    if (r.x >= grid.length || r.y >= grid(r.x).length) {
      controller.send(JsonConverter.toJson(Map("message_type" -> "error", "message" -> "X or Y coordinates out of bound !")))
      return
    }
    if (grid(r.x)(r.y) != 20) {
      controller.send(JsonConverter.toJson(Map("message_type" -> "error", "message" -> "No tower to remove!")))
      return
    }
    val tower = towersMap.get((r.x, r.y))
    tower match {
      case Some(t) =>
        val cost = computeTotalCost(Some(t.id), 0.75)
        money += cost
        controller.send(JsonConverter.toJson(Map("message_type" -> "money", "message" -> money)))
        grid(r.x)(r.y) = 0
        towersMap.remove((r.x, r.y))
        controller.send("{\"message_type\":\"tower_delete\",\"message\":" + r + "}")
      case _ => println("removeTower: should never happen !")
    }
  }

  private def computeTotalCost(towerType: Option[String], ratio: Double): Int = {
    towerType match {
      case None => 0
      case Some(_type) => towers.get(_type) match {
        case Some(tower) =>
          (tower.cost * ratio).toInt + computeTotalCost(tower.previous, ratio)
        case _ => 0
      }
    }
  }
  private def checkPause(): Unit = while(isPaused) Thread.sleep(100)
  private def moveMonsters(): Unit = {
    checkPause()
    val monstersToRemove: ListBuffer[MonsterInGame] = ListBuffer()
    val monstersMove: ListBuffer[JsValue] = ListBuffer()
    for (m <- Random.shuffle(monstersList)) {
      breakable {
        for (_ <- 0.until(m.speed)) {
          m.current_move += 1
          if (m.x == m.path.last._1 && m.y == m.path.last._2) {
            lives -= m.damage
            controller.send(JsonConverter.toJson(Map("message_type" -> "lives", "message" -> lives.toString)))
            m.x = -1
            m.y = -1
            monstersMove.append(m.copy().toJson)
            monstersToRemove.append(m)
            break
          } else {
            try {
              val nextMove = m.path(m.current_move)
              m.x = nextMove._1
              m.y = nextMove._2
            } catch {
              case e: Exception =>
              // Monster with speed higher than one, already out of the map
              // Should not happen, but just in case, discard error
              println(e.getMessage)
            }
          }
        }
        monstersMove.append(m.copy().toJson)
        m.last_x = m.x
        m.last_y = m.y
      }
    }
    if (monstersMove.toList.nonEmpty) {
      controller.send(JsonConverter.toJson(Map("message_type" -> "monster_move", "message" -> monstersMove.toList)))
    }
    for (m <- monstersToRemove.reverse) {
      monstersList -= m
    }
  }

  private def isInRange(x: Int, y: Int, range: Int): Boolean = {
    (for (m <- monstersList) yield ((x - range) <= m.x && m.x <= (x + range)) && (((y - range) <= m.y) && (m.y <= (y + range)))).count(_ == true) > 0
  }

  private def fireTowers(): Unit = {
    checkPause()
    val towersFire: ListBuffer[JsValue] = ListBuffer()
    for ((x, y) <- Random.shuffle(towersMap.keySet)) {
      val tower = towersMap.get((x, y))
      tower match {
        case Some(t) =>
          var shots = 0
          if (monstersList.isEmpty) {
            return
          }
          while (shots < t.speed && isInRange(x, y, t.range)) {
            val monstersToRemove: ListBuffer[MonsterInGame] = ListBuffer()
            breakable {
              for (m <- monstersList) {
                if (((x - t.range) <= m.x && m.x <= (x + t.range)) && (((y - t.range) <= m.y) && (m.y <= (y + t.range)))) {
                  shots += 1
                  m.currentHealth -= t.power
                  towersFire.append(Map("tower" -> Map("tower_type" -> t.id, "x" -> x, "y" -> y).toJson, "monster" -> m.toJson).toJson)
                  if (m.currentHealth <= 0) {
                    monstersToRemove.append(m)
                    money += m.money
                    controller.send(JsonConverter.toJson(Map("message_type" -> "money", "message" -> money.toString)))
                    controller.send("{\"message_type\":\"monster_death\",\"message\":" + m + "}")
                    break
                  }
                }
                if (shots >= t.speed) {
                  break
                }
              }
            }
            for (m <- monstersToRemove.reverse) {
              monstersList -= m
            }
            if (towersFire.toList.nonEmpty) {
              controller.send(JsonConverter.toJson(Map("message_type" -> "monster_hit", "message" -> towersFire.toList)))
            }
          }
        case _ => println("fireTowers: should never happen !")
      }
    }
  }
  private def gameLost(): Unit = {
    controller.send(JsonConverter.toJson(Map("message_type" -> "game_lost", "message" -> "Game lost !")))
  }
  private def gameWon(): Unit = {
    controller.send(JsonConverter.toJson(Map("message_type" -> "game_won", "message" -> "Game won !")))
  }
}