package engines

import java.io.PrintStream
import java.net.SocketException
import spray.json._

import models.JsonConverter
import models.JsonImplicit._
import models.Model._
import scala.io.BufferedSource

abstract class PlayerEngine(in: BufferedSource, out: PrintStream) extends Thread {

  val mGame = GameEngineNormal(this)

  def clientRequest(request: Request): Unit = request.request_type match {
    case "resume" =>
      mGame.resumeGame()
      send(JsonConverter.toJson(Map("message_type" -> "game_resumed", "message" -> "Game resumed !")))
    case "pause" =>
      mGame.pauseGame()
      send(JsonConverter.toJson(Map("message_type" -> "game_paused", "message" -> "Game paused !")))
    case "stop" =>
      mGame.stopGame()
      send(JsonConverter.toJson(Map("message_type" -> "game_stopped", "message" -> "Game stopped !")))
    case "build_tower" =>
      try {
        val r = request.request.convertTo[RequestBuild]
        mGame.buildTower(r)
      } catch {
        case _: Exception =>
          send(JsonConverter.toJson(Map("message_type" -> "error", "message" -> "Malformed JSON -> {`tower_type`: ..., `x`: ..., `y`: ...}")))
      }
    case "update_tower" =>
      try {
        val r = request.request.convertTo[RequestUpdate]
        mGame.updateTower(r)
      } catch {
        case _: Exception =>
          send(JsonConverter.toJson(Map("message_type" -> "error", "message" -> "Malformed JSON -> {`tower_type`: ..., `x`: ..., `y`: ...}")))
      }
    case "remove_tower" =>
      try {
        val r = request.request.convertTo[RequestDelete]
        mGame.removeTower(r)
      } catch {
        case _: Exception =>
          send(JsonConverter.toJson(Map("message_type" -> "error", "message" -> "Malformed JSON -> {`x`: ..., `y`: ...}")))
      }
    case _ =>
      send("Unknown request: " + request)
  }

  def send(message: String): Unit

  override def run(): Unit = {
    try {
      for (line <- in.getLines()) {
        try {
          val json = line.parseJson
          clientRequest(json.convertTo[Request])
        } catch {
          case _: Exception =>
            send(JsonConverter.toJson(Map("message_type" -> "error", "message" -> "Malformed JSON -> {`request_type`: ..., `request`: {} }")))
        }
      }
    } catch {
      case e: SocketException => println(e.getMessage)
    }
    mGame.stopGame()
    println("Client excited !")
  }
}