package utils

import scala.collection.immutable.Queue
import scala.util.Random

case class PathSolver(grid: Array[Array[Int]], startX: Int, startY: Int, endX: Int, endY: Int) {

  def validAndTraversable(isTraversable: Int => Boolean, grid: Array[Array[Int]], xy: (Int, Int)): Boolean = {
    (xy._1 >= 0) && (xy._1 < grid.length) && (xy._2 >= 0) && (xy._2 < grid(0).length)&& isTraversable(grid(xy._1)(xy._2))
  }

  private def getPath(grid: Array[Array[Option[Option[(Int, Int)]]]], end: (Int, Int)) = {
    def acc(grid: Array[Array[Option[Option[(Int, Int)]]]], path: List[(Int, Int)], xy: (Int, Int)): List[(Int, Int)] = {
      val (x, y) = xy
      grid(x)(y) match {
        case Some(Some(prevXY)) => acc(grid, xy :: path, prevXY)
        case Some(None) => xy :: path
        case None => Nil
      }
    }
    acc(grid, Nil, end)
  }

  private def solveLoop(isFinish: ((Int, Int)) => Boolean, isTraversable: Int => Boolean, grid: Array[Array[Int]],
                        queue: Queue[(Int, Int)], indexGrid: Array[Array[Option[Option[(Int, Int)]]]]): List[(Int, Int)] = {
    if (queue.isEmpty) {
      Nil
    } else {
      val (currentXY, rest) = queue.dequeue
      val (x, y) = currentXY
      if (isFinish(currentXY)) {
        val p = getPath(indexGrid, currentXY)
        p
      }
      else {
        val neighbors = Random.shuffle(List((x + 1, y), (x, y + 1), (x - 1, y), (x, y - 1)))
        val unvisited = neighbors.filter { case ij@(i, j) => validAndTraversable(isTraversable, grid, ij) && indexGrid(i)(j).isEmpty }
        for ((i, j) <- unvisited) {
          indexGrid(i)(j) = Some(Some(currentXY))
        }
        val updatedQueue = rest ++ Queue(unvisited: _*)
        solveLoop(isFinish, isTraversable, grid, updatedQueue, indexGrid)
      }
    }
  }

  private def findPath(start: (Int, Int), isFinish: ((Int, Int)) => Boolean,
                       isTraversable: Int => Boolean, grid: Array[Array[Int]]) = {
    if (validAndTraversable(isTraversable, grid, start)) {
      val (x, y) = start
      val indexGrid = Array.fill[Option[Option[(Int, Int)]]](grid.length, grid(0).length)(None)
      indexGrid(x)(y) = Some(None)
      solveLoop(isFinish, isTraversable, grid, Queue(start), indexGrid)
    }
    else {
      Nil
    }
  }

  def solve(): List[(Int, Int)] =
    findPath((startX, startY), (xy: (Int, Int)) => xy == (endX, endY), (x: Int) => x == 1 || x == 2 || x == 3 || x == 4 || x == 5 || x == 6 || x == 7 || x == 8 || x == 9 || x == 10 || x == 11, grid)
}
