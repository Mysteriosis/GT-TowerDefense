# GT-TowerDefense

## Requirements
- Java 8 & JavaFX for the client
- SBT for the server (Scala should be downloaded automatically)

## Execution
### Server
- Simply execute the command `sbt run` in the folder `server` (where there is a file `build.sbt`).
This should download all the dependencies and execute the server.

### Client
- Simply execute the command `java -jar client.jar` in the folder `client/executable`.
This should launch the client.

## Multiplayer Game
Currently, as the server does not have a fixed IP, the clients connect to localhost.
This implies that we can play the multiplayer game only in local, launching two instances of the client.

## External repositories
[edufgf/TowerDefense](https://github.com/edufgf/TowerDefense/tree/master/Tower%20Defense): Used for the GUI.
